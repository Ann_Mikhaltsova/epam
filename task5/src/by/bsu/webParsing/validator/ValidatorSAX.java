package by.bsu.webParsing.validator;

import java.io.File;
import java.io.IOException;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

public class ValidatorSAX {
    private static Logger LOG = LogManager.getLogger(ValidatorSAX.class.getName());

    public void validate(String projectRoot) {
        String filename = "data/papers.xml";
        String schemaname = "data/periodicals.xsd";
        Schema schema = null;
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        try {
            schema = factory.newSchema(new File(projectRoot + schemaname));
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setSchema(schema);
            SAXParser parser = spf.newSAXParser();
            parser.parse(projectRoot + filename, new PeriodicalsErrorHandler(LOG));
            LOG.info("success validation");
        } catch (ParserConfigurationException e) {
            LOG.error(filename + " config error: " + e.getMessage());
        } catch (SAXException e) {
            LOG.error(filename + " SAX error: " + e.getMessage());
        } catch (IOException e) {
            LOG.error("I/O error: " + e.getMessage());
        }
    }
}
