package by.bsu.webParsing.server;

import by.bsu.webParsing.builder.PeriodicalsBuilderFactory;
import by.bsu.webParsing.entity.Periodicals;
import by.bsu.webParsing.parser.AbstractPeriodicalsBuilder;
import by.bsu.webParsing.validator.ValidatorSAX;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet("/ButtonPressAction")

public class ParsingServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(ParsingServlet.class.getName());

    public ParsingServlet() {
        super();
    }

    public void init() throws ServletException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ValidatorSAX validatorSAX = new ValidatorSAX();
        String projectRoot = getServletContext().getRealPath("/");
        validatorSAX.validate(projectRoot);
        LOG.info("take parser");
        String parserName = request.getParameter("parser");
        PeriodicalsBuilderFactory periodicalsFactory = new PeriodicalsBuilderFactory();
        AbstractPeriodicalsBuilder builder = periodicalsFactory.createPeriodicalsBuilder(parserName);
        builder.buildSetPeriodicals(projectRoot + "data/papers.xml");
        Set<Periodicals> periodicals = builder.getPeriodicals();
        request.setAttribute("periodicals", periodicals);
        LOG.info("go to another jsp-page");
        request.getRequestDispatcher("jsp/output.jsp").forward(request,response);
    }

    public void destroy() {
        super.destroy();
    }
}
