package by.bsu.webParsing.parser;

import by.bsu.webParsing.entity.Chars;
import by.bsu.webParsing.entity.Magazine;
import by.bsu.webParsing.entity.Newspaper;
import by.bsu.webParsing.entity.Periodicals;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Set;

public class PeriodicalsDOMBuilder extends AbstractPeriodicalsBuilder {
    private static final Logger LOG = LogManager.getLogger(PeriodicalsDOMBuilder.class.getName());
    private DocumentBuilder docBuilder;

    public PeriodicalsDOMBuilder() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error("Parse configuration's error: " + e);
        }
    }

    public PeriodicalsDOMBuilder(Set<Periodicals> periodicals) {
        super(periodicals);
    }

    @Override
    public void buildSetPeriodicals(String fileName) {
        Document doc = null;
        try {
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList periodicalsList = root.getElementsByTagName("newspaper");
            for (int i = 0; i < periodicalsList.getLength(); i++) {
                Element periodicalsElement = (Element) periodicalsList.item(i);
                Newspaper newspaper = buildNewspaper(periodicalsElement);
                periodicals.add(newspaper);
            }
            periodicalsList = root.getElementsByTagName("magazine");
            for (int i = 0; i < periodicalsList.getLength(); i++) {
                Element periodicalsElement = (Element) periodicalsList.item(i);
                Magazine magazine = buildMagazine(periodicalsElement);
                periodicals.add(magazine);
            }
        } catch (IOException e) {
            LOG.error("File error or I/O error: " + e);
        } catch (SAXException e) {
            LOG.error("Parsing failure: " + e);
        }
    }

    private Newspaper buildNewspaper(Element periodicalsElement) {
        Newspaper newspaper = new Newspaper();
        buildPeriodicals(periodicalsElement, newspaper);
        newspaper.setTelevisionProgram(Boolean.valueOf(
                getElementTextContent(periodicalsElement, "television-program")));
        return newspaper;
    }

    private Magazine buildMagazine(Element periodicalsElement) {
        Magazine magazine = new Magazine();
        buildPeriodicals(periodicalsElement, magazine);
        magazine.setGloss(Boolean.valueOf(getElementTextContent(periodicalsElement, "gloss")));
        return magazine;
    }

    private void buildPeriodicals(Element periodicalsElement, Periodicals periodicals) {
        periodicals.setTitle(periodicalsElement.getAttribute("title"));
        periodicals.setIndex(getElementTextContent(periodicalsElement, "index"));
        periodicals.setPublicationCountry(getElementTextContent(periodicalsElement, "publication-country"));
        periodicals.setPublicationPeriod(getElementTextContent(periodicalsElement, "publication-period"));
        periodicals.setChars(new Chars());
        Chars chars = periodicals.getChars();
        Element charsElement = (Element) periodicalsElement.getElementsByTagName("chars").item(0);
        chars.setColour(Boolean.valueOf(getElementTextContent(charsElement, "colour")));
        chars.setPageVolume(BigInteger.valueOf(Long.parseLong
                (getElementTextContent(charsElement, "page-volume"))));
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}
