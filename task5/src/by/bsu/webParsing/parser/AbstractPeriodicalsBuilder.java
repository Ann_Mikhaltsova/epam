package by.bsu.webParsing.parser;

import by.bsu.webParsing.entity.Periodicals;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractPeriodicalsBuilder {
    protected Set<Periodicals> periodicals;

    public AbstractPeriodicalsBuilder() {
        periodicals = new HashSet<Periodicals>();
    }

    public AbstractPeriodicalsBuilder(Set<Periodicals> periodicals) {
        this.periodicals = periodicals;
    }

    public Set<Periodicals> getPeriodicals() {
        return periodicals;
    }

    abstract public void buildSetPeriodicals(String fileName);
}
