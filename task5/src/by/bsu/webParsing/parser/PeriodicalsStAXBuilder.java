package by.bsu.webParsing.parser;

import by.bsu.webParsing.entity.Chars;
import by.bsu.webParsing.entity.Magazine;
import by.bsu.webParsing.entity.Newspaper;
import by.bsu.webParsing.entity.Periodicals;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Set;

public class PeriodicalsStAXBuilder extends AbstractPeriodicalsBuilder {
    private static final Logger LOG = LogManager.getLogger(PeriodicalsStAXBuilder.class.getName());
    private XMLInputFactory inputFactory;

    public PeriodicalsStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    public PeriodicalsStAXBuilder(Set<Periodicals> periodicals) {
        super(periodicals);
    }

    public Set<Periodicals> getPeriodicals() {
        return periodicals;
    }

    @Override
    public void buildSetPeriodicals(String fileName) {
        FileInputStream inputStream = null;
        XMLStreamReader reader;
        String name;
        try {
            inputStream = new FileInputStream(new File(fileName));
            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (PeriodicalsEnum.valueOf(name.toUpperCase()) == PeriodicalsEnum.NEWSPAPER) {
                        Newspaper newspaper = buildNewspaper(reader);
                        periodicals.add(newspaper);
                    } else if (PeriodicalsEnum.valueOf(name.toUpperCase()) == PeriodicalsEnum.MAGAZINE) {
                        Magazine magazine = buildMagazine(reader);
                        periodicals.add(magazine);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            LOG.error("StAX webParsing error! " + ex.getMessage());
        } catch (FileNotFoundException ex) {
            LOG.error("File " + fileName + " not found! " + ex);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                LOG.error("Impossible close file " + fileName + " : " + e);
            }
        }
    }

    private Newspaper buildNewspaper(XMLStreamReader reader) throws XMLStreamException {
        Newspaper newspaper = new Newspaper();
        newspaper = (Newspaper) buildPeriodicals(reader, newspaper);
        return newspaper;
    }

    private Magazine buildMagazine(XMLStreamReader reader) throws XMLStreamException {
        Magazine magazine = new Magazine();
        magazine = (Magazine) buildPeriodicals(reader, magazine);
        return magazine;
    }

    private Periodicals buildPeriodicals(XMLStreamReader reader, Periodicals periodicals) throws XMLStreamException {
        periodicals.setChars(new Chars());
        periodicals.setTitle(reader.getAttributeValue(null, PeriodicalsEnum.TITLE.getValue()));
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (PeriodicalsEnum.valueOf(name.toUpperCase().replace('-', '_'))) {
                        case INDEX:
                            periodicals.setIndex(getXMLText(reader));
                            break;
                        case PUBLICATION_COUNTRY:
                            periodicals.setPublicationCountry(getXMLText(reader));
                            break;
                        case PUBLICATION_PERIOD:
                            periodicals.setPublicationPeriod(getXMLText(reader));
                            break;
                        case CHARS:
                            periodicals.setChars(getXMLChars(reader));
                            break;
                        case TELEVISION_PROGRAM:
                            Newspaper newspaper = (Newspaper) periodicals;
                            newspaper.setTelevisionProgram(Boolean.valueOf(getXMLText(reader)));
                            break;
                        case GLOSS:
                            Magazine magazine = (Magazine) periodicals;
                            magazine.setGloss(Boolean.valueOf(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (PeriodicalsEnum.valueOf(name.toUpperCase().replace('-', '_')) == PeriodicalsEnum.NEWSPAPER
                            || PeriodicalsEnum.valueOf(name.toUpperCase().replace('-', '_')) == PeriodicalsEnum.MAGAZINE) {
                        return periodicals;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag" + reader.getLocalName());
    }

    private Chars getXMLChars(XMLStreamReader reader) throws XMLStreamException {
        Chars chars = new Chars();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (PeriodicalsEnum.valueOf(name.toUpperCase().replace('-', '_'))) {
                        case COLOUR:
                            chars.setColour(Boolean.valueOf(getXMLText(reader)));
                            break;
                        case PAGE_VOLUME:
                            chars.setPageVolume(BigInteger.valueOf(Long.parseLong(getXMLText(reader))));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (PeriodicalsEnum.valueOf(name.toUpperCase().replace('-', '_')) ==
                            PeriodicalsEnum.CHARS) {
                        return chars;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag VisualParameters");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
