package by.bsu.webParsing.parser;

import by.bsu.webParsing.entity.Periodicals;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;

public class PeriodicalsSAXBuilder extends AbstractPeriodicalsBuilder {
    private static final Logger LOG = LogManager.getLogger(PeriodicalsSAXBuilder.class.getName());
    private PeriodicalsHandler ph;
    private XMLReader reader;

    public PeriodicalsSAXBuilder() {
        ph = new PeriodicalsHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(ph);
        } catch (SAXException e) {
            LOG.error("SAX'parser error: " + e);
        }
    }

    public PeriodicalsSAXBuilder(Set<Periodicals> periodicals) {
        super(periodicals);
    }

    public Set<Periodicals> getPeriodicals() {
        return periodicals;
    }

    @Override
    public void buildSetPeriodicals(String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException e) {
            LOG.error("SAX'parser error: " + e);
        } catch (IOException e) {
            LOG.error("I/О-stream's error: " + e);
        }
        periodicals = ph.getPeriodicals();
    }
}
