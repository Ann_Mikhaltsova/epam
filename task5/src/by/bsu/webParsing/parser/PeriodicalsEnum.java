package by.bsu.webParsing.parser;

public enum PeriodicalsEnum {
    PAPERS("papers"),
    NEWSPAPER("newspaper"),
    MAGAZINE("magazine"),
    TITLE("title"),
    INDEX("index"),
    PUBLICATION_COUNTRY("publication-country"),
    PUBLICATION_PERIOD("publication-period"),
    COLOUR("colour"),
    PAGE_VOLUME("page-volume"),
    TELEVISION_PROGRAM("television-program"),
    GLOSS("gloss"),
    CHARS("chars");
    private String value;

    private PeriodicalsEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
