package by.bsu.webParsing.parser;

import by.bsu.webParsing.entity.Chars;
import by.bsu.webParsing.entity.Magazine;
import by.bsu.webParsing.entity.Newspaper;
import by.bsu.webParsing.entity.Periodicals;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigInteger;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class PeriodicalsHandler extends DefaultHandler {
    private Set<Periodicals> periodicals;
    private Periodicals current = null;
    private PeriodicalsEnum currentEnum = null;
    private EnumSet<PeriodicalsEnum> withText;

    public PeriodicalsHandler() {
        periodicals = new HashSet<Periodicals>();
        withText = EnumSet.range(PeriodicalsEnum.INDEX, PeriodicalsEnum.GLOSS);
    }

    public Set<Periodicals> getPeriodicals() {
        return periodicals;
    }

    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        if ("newspaper".equals(localName)) {
            current = new Newspaper();
            current.setTitle(attrs.getValue(0));
            current.setChars(new Chars());
        } else if ("magazine".equals(localName)) {
            current = new Magazine();
            current.setTitle(attrs.getValue(0));
            current.setChars(new Chars());
        } else {
            PeriodicalsEnum temp = PeriodicalsEnum.valueOf(localName.toUpperCase().replace('-', '_'));
            if (withText.contains(temp)) {
                currentEnum = temp;
            }
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if ("newspaper".equals(localName) || "magazine".equals(localName)) {
            periodicals.add(current);
        }
    }

    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case INDEX:
                    current.setIndex(s);
                    break;
                case PUBLICATION_COUNTRY:
                    current.setPublicationCountry(s);
                    break;
                case PUBLICATION_PERIOD:
                    current.setPublicationPeriod(s);
                    break;
                case COLOUR:
                    current.getChars().setColour(Boolean.valueOf(s));
                    break;
                case PAGE_VOLUME:
                    current.getChars().setPageVolume(BigInteger.valueOf(Long.parseLong(s)));
                    break;
                case TELEVISION_PROGRAM:
                    Newspaper newspaperHelper = (Newspaper) current;
                    newspaperHelper.setTelevisionProgram(Boolean.valueOf(s));
                    current = newspaperHelper;
                    break;
                case GLOSS:
                    Magazine magazineHelper = (Magazine) current;
                    magazineHelper.setGloss(Boolean.valueOf(s));
                    current = magazineHelper;
                    break;
                default:
                    throw new EnumConstantNotPresentException(
                            currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }
}
