package by.bsu.webParsing.builder;

import by.bsu.webParsing.parser.AbstractPeriodicalsBuilder;
import by.bsu.webParsing.parser.PeriodicalsDOMBuilder;
import by.bsu.webParsing.parser.PeriodicalsSAXBuilder;
import by.bsu.webParsing.parser.PeriodicalsStAXBuilder;

public class PeriodicalsBuilderFactory {
    private enum TypeParser {
        SAX,
        STAX,
        DOM
    }

    public AbstractPeriodicalsBuilder createPeriodicalsBuilder(String typeParser) {
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type) {
            case SAX:
                return new PeriodicalsSAXBuilder();
            case STAX:
                return new PeriodicalsStAXBuilder();
            case DOM:
                return new PeriodicalsDOMBuilder();
            default:
                throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
        }
    }
}
