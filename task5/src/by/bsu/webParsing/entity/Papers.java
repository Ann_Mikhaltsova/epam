package by.bsu.webParsing.entity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "periodicals"
})
@XmlRootElement(name = "papers")
public class Papers {

    @XmlElementRef(name = "periodicals", namespace = "http://www.bsu.by/webParsing/entity", type = JAXBElement.class)
    protected List<JAXBElement<? extends Periodicals>> periodicals;

    public List<JAXBElement<? extends Periodicals>> getPeriodicals() {
        if (periodicals == null) {
            periodicals = new ArrayList<JAXBElement<? extends Periodicals>>();
        }
        return this.periodicals;
    }
}
