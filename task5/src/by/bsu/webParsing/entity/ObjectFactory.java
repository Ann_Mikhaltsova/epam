package by.bsu.webParsing.entity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


@XmlRegistry
public class ObjectFactory {

    private final static QName _Magazine_QNAME = new QName("http://www.bsu.by/webParsing/entity", "magazine");
    private final static QName _Periodicals_QNAME = new QName("http://www.bsu.by/webParsing/entity", "periodicals");
    private final static QName _Newspaper_QNAME = new QName("http://www.bsu.by/webParsing/entity", "newspaper");

    public ObjectFactory() {
    }

    public Newspaper createNewspaper() {
        return new Newspaper();
    }

    public Periodicals createPeriodicals() {
        return new Periodicals();
    }

    public Magazine createMagazine() {
        return new Magazine();
    }

    public Papers createPapers() {
        return new Papers();
    }

    public Chars createChars() {
        return new Chars();
    }

    @XmlElementDecl(namespace = "http://www.bsu.by/webParsing/entity", name = "magazine", substitutionHeadNamespace = "http://www.bsu.by/webParsing/entity", substitutionHeadName = "periodicals")
    public JAXBElement<Magazine> createMagazine(Magazine value) {
        return new JAXBElement<Magazine>(_Magazine_QNAME, Magazine.class, null, value);
    }

    @XmlElementDecl(namespace = "http://www.bsu.by/webParsing/entity", name = "periodicals")
    public JAXBElement<Periodicals> createPeriodicals(Periodicals value) {
        return new JAXBElement<Periodicals>(_Periodicals_QNAME, Periodicals.class, null, value);
    }

    @XmlElementDecl(namespace = "http://www.bsu.by/webParsing/entity", name = "newspaper", substitutionHeadNamespace = "http://www.bsu.by/webParsing/entity", substitutionHeadName = "periodicals")
    public JAXBElement<Newspaper> createNewspaper(Newspaper value) {
        return new JAXBElement<Newspaper>(_Newspaper_QNAME, Newspaper.class, null, value);
    }
}
