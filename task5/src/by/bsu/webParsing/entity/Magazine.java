package by.bsu.webParsing.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Magazine", propOrder = {
    "gloss"
})
public class Magazine extends Periodicals {

    @XmlElement(defaultValue = "true")
    protected boolean gloss;

    public boolean isGloss() {
        return gloss;
    }

    public void setGloss(boolean value) {
        this.gloss = value;
    }
}
