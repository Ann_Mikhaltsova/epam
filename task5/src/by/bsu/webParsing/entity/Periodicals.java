package by.bsu.webParsing.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Periodicals", propOrder = {
    "index",
    "publicationCountry",
    "publicationPeriod",
    "chars"
})
@XmlSeeAlso({
    Newspaper.class,
    Magazine.class
})
public class Periodicals {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String index;
    @XmlElement(name = "publication-country", required = true)
    protected String publicationCountry;
    @XmlElement(name = "publication-period", required = true)
    protected String publicationPeriod;
    @XmlElement(required = true)
    protected Chars chars;
    @XmlAttribute(name = "title")
    protected String title;

    public String getIndex() {
        return index;
    }

    public void setIndex(String value) {
        this.index = value;
    }

    public String getPublicationCountry() {
        return publicationCountry;
    }

    public void setPublicationCountry(String value) {
        this.publicationCountry = value;
    }

    public String getPublicationPeriod() {
        return publicationPeriod;
    }

    public void setPublicationPeriod(String value) {
        this.publicationPeriod = value;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars value) {
        this.chars = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String value) {
        this.title = value;
    }
}
