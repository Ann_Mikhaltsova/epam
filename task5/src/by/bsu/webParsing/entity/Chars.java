package by.bsu.webParsing.entity;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Chars", propOrder = {
    "colour",
    "pageVolume"
})
public class Chars {
    protected boolean colour;
    @XmlElement(name = "page-volume", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger pageVolume;

    public boolean isColour() {
        return colour;
    }

    public void setColour(boolean value) {
        this.colour = value;
    }

    public BigInteger getPageVolume() {
        return pageVolume;
    }

    public void setPageVolume(BigInteger value) {
        this.pageVolume = value;
    }
}
