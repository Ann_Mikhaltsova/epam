package by.bsu.webParsing.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Newspaper", propOrder = {
    "televisionProgram"
})
public class Newspaper extends Periodicals {

    @XmlElement(name = "television-program", defaultValue = "true")
    protected boolean televisionProgram;

    public boolean isTelevisionProgram() {
        return televisionProgram;
    }

    public void setTelevisionProgram(boolean value) {
        this.televisionProgram = value;
    }
}
