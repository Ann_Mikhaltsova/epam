<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Result</title>
    <LINK rel="stylesheet" href="${path}/css/style.css">
</head>
<body>
<table>
    <c:set var="newspaper" value="by.bsu.webParsing.entity.Newspaper" />
    <c:set var="magazine" value="by.bsu.webParsing.entity.Magazine" />
    <caption>Periodicals</caption>
    <tr>
        <th rowspan="2">Type</th>
        <th rowspan="2">Title</th>
        <th rowspan="2">Index</th>
        <th rowspan="2">Publication country</th>
        <th rowspan="2">Publication period</th>
        <th colspan="2" align="center">Chars</th>
        <th rowspan="2" align="center">Television program/Gloss</th>
    </tr>
    <tr>
        <th>Colour</th>
        <th>Page volume</th>
    </tr>
    <c:forEach var="paper" items="${periodicals}" varStatus="status">
        <tr>
            <c:if test="${paper.getClass().name eq newspaper}">
                <td>Newspaper</td>
            </c:if>
            <c:if test="${paper.getClass().name eq magazine}">
                <td>Magazine</td>
            </c:if>
            <td>${paper.title}</td>
            <td>${paper.index}</td>
            <td>${paper.publicationCountry}</td>
            <td>${paper.publicationPeriod}</td>
            <td>${paper.chars.colour}</td>
            <td>${paper.chars.pageVolume}</td>
            <c:if test="${paper.getClass().name eq newspaper}">
                <td>${paper.televisionProgram}</td>
            </c:if>
            <c:if test="${paper.getClass().name eq magazine}">
                <td>${paper.gloss}</td>
            </c:if>
        </tr>
    </c:forEach>
</table>
<form action="index.jsp">
    <input class="backButton" type="submit" value="Back" >
    <!--<input class="backButton" type="submit" value="Back" onclick="history.back()">-->

</form>
</body>
</html>
