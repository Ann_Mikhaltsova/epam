<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
      <title>Task 5: XML Parsing with Web</title>
      <LINK rel="stylesheet" href="${path}/css/style.css">
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <h1>Task 5: XML Parsing with Web</h1>
            </div>
            <div id="content">
                <p>Choose one parser from list:</p>
                <form action="ButtonPressAction">
                    <input type="submit" name="parser" value="SAX">
                </form>
                <form action="ButtonPressAction">
                    <input type="submit" name="parser" value="StAX">
                </form>
                <form action="ButtonPressAction">
                    <input type="submit" name="parser" value="DOM">
                </form>
            </div>
            <div id="footer">
                Ann Mikhaltsova © 2016
            </div>
        </div>
    </body>
</html>