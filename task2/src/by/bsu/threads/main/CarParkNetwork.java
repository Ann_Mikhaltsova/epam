package by.bsu.threads.main;

import by.bsu.threads.entity.Car;
import by.bsu.threads.entity.CarPark;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CarParkNetwork {
    private static final Logger LOG = Logger.getLogger(CarParkNetwork.class);

    public static void main(String[] args) {
        ArrayList<CarPark> parks = new ArrayList<>();
        for(int i = 0; i < 3; i++) {
            parks.add(CarPark.getCarPark());
        }
        int index = 0;
        LinkedList<Car> carsToAnotherPark = new LinkedList<>();
        do {
            if (index == 0) {
                parks.get(index).makeCarQueue();
            } else {
                parks.get(index % 3).getCarsTo().clear();
                parks.get(index % 3).makeCarQueueForOnceMoreUse(carsToAnotherPark);
            }
            ExecutorService es = Executors.newFixedThreadPool(parks.get(index % 3).getCarsTo().size());
            for(int i = 0; i < parks.get(index % 3).getCarsTo().size(); i++) {
                es.submit(parks.get(index % 3).getCarsTo().get(i));
            }
            es.shutdown();
            carsToAnotherPark.clear();
            carsToAnotherPark.addAll(parks.get(index % 3).getCarsToAnotherPark());
        } while (!parks.get(index++ % 3).getCarsToAnotherPark().isEmpty());
        LOG.info("All cars have been parked!");
    }
}
