package by.bsu.threads.entity;

import by.bsu.threads.creator.ParkCreator;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class CarPark {
    private static int counter = 3;
    private static final int STALL_NUMBER = 3;
    private final Semaphore semaphore = new Semaphore(STALL_NUMBER, true);
    private LinkedList<Car> carsTo = new LinkedList<>();
    private LinkedList<Car> carsToAnotherPark = new LinkedList<>();
    private static class Helper {
        private static CarPark carPark = null;
        public static CarPark createCarPark() {
            if(counter-- > 0) {
                carPark = new CarPark();
            }
            return carPark;
        }
    }

    private CarPark() {
    }

    public static CarPark getCarPark() {
        return Helper.createCarPark();
    }

    public static CarPark getCarParkElement() {
        return Helper.carPark;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public LinkedList<Car> getCarsTo() {
        return carsTo;
    }

    public LinkedList<Car> getCarsToAnotherPark() {
        return carsToAnotherPark;
    }

    public void makeCarQueue() {
        carsTo = new ParkCreator().makeCarList(this);
    }

    public void makeCarQueueForOnceMoreUse(LinkedList<Car> carsToAP) {
        for(int i = 0; i < carsToAP.size(); i++) {
            Car car = carsToAP.get(i);
            carsTo.add(new Car(car.getCarName(), car.getParkTime(), car.getWaitingTime(),
                    car.getOccupiedPlaceNumber(), this));
        }
    }
}
