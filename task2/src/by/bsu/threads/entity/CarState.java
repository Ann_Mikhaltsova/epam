package by.bsu.threads.entity;

public enum CarState {
    WAIT_PARK_PLACE,
    IN_PARK,
    OUT_OF_PARK
}
