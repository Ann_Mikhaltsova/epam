package by.bsu.threads.entity;

import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class Car extends Thread{
    private static final Logger LOG = Logger.getLogger(Car.class);
    private String carName;
    private int parkTime;
    private int waitingTime;
    private CarState state = CarState.WAIT_PARK_PLACE;
    private int occupiedPlaceNumber;
    private CarPark carPark;

    private Car() {
    }

    public Car(String carName, int parkTime, int waitingTime, int occupiedPlaceNumber, CarPark carPark) {
        this.carName = carName;
        this.parkTime = parkTime;
        this.waitingTime = waitingTime;
        this.occupiedPlaceNumber = occupiedPlaceNumber;
        this.carPark = carPark;
    }

    public String getCarName() {
        return carName;
    }

    public int getParkTime() {
        return parkTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public int getOccupiedPlaceNumber() {
        return occupiedPlaceNumber;
    }

    public void setState(CarState state) {
        this.state = state;
    }

    public void park() {
        setState(CarState.IN_PARK);
        LOG.info(carName + " is " + state + " for " + parkTime + " sec and occupies " + occupiedPlaceNumber +
                " park places");
        try {
            TimeUnit.SECONDS.sleep(parkTime);
        } catch (InterruptedException ex) {
            LOG.warn("Car parking has been interrupted");
        }
    }

    public void driveAway() {
        setState(CarState.OUT_OF_PARK);
    }

    public void findAnotherPark() {
        setState(CarState.WAIT_PARK_PLACE);
        LOG.info(carName + " goes to another park");
    }

    @Override
    public void run() {
        try {
            LOG.info(carName + " is " + state + " before semaphore");
            if(CarPark.getCarParkElement().getSemaphore().tryAcquire(occupiedPlaceNumber,
                    waitingTime, TimeUnit.SECONDS)) {
                CarPark.getCarParkElement().getSemaphore().acquire(occupiedPlaceNumber);
                park();
                driveAway();
            } else {
                findAnotherPark();
                carPark.getCarsToAnotherPark().add(this);
            }

        } catch (InterruptedException ex) {
            LOG.warn("Visit to car park has been interrupted");
        } finally {
            LOG.info(carName + " is " + state);
            CarPark.getCarParkElement().getSemaphore().release(occupiedPlaceNumber);
        }
    }
}
