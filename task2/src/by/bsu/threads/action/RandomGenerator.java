package by.bsu.threads.action;

import java.util.Random;

public class RandomGenerator {
    private static final int MAX_CAR_POSSIBLE_NUMBER = 10000;
    private static final int MIN_CAR_POSSIBLE_NUMBER = 1;
    private static final int MAX_PARK_TIME = 10;
    private static final int MIN_PARK_TIME = 3;
    private static final int MAX_WAITING_TIME = 5;
    private static final int MAX_OCCUPIED_PLACE_NUMBER = 3;
    private static final int MIN_OCCUPIED_PLACE_NUMBER = 1;
    private Random random = new Random();

    public String thinkCarName() {
        int serialNumber = random.nextInt(MAX_CAR_POSSIBLE_NUMBER - MIN_CAR_POSSIBLE_NUMBER) +
                MIN_CAR_POSSIBLE_NUMBER;
        String carName = "Car ";
        return carName + serialNumber;
    }

    public int thinkParkTime() {
        int parkTime = random.nextInt(MAX_PARK_TIME - MIN_PARK_TIME) + MIN_PARK_TIME;
        return parkTime;
    }

    public int thinkWaitingTime() {
        int waitTime = random.nextInt(MAX_WAITING_TIME);
        return waitTime;
    }

    public int thinkOccupiedPlaceNumber() {
        int occupiedPlaceNumber = random.nextInt(MAX_OCCUPIED_PLACE_NUMBER - MIN_OCCUPIED_PLACE_NUMBER) +
                MIN_OCCUPIED_PLACE_NUMBER;
        return occupiedPlaceNumber;
    }
}
