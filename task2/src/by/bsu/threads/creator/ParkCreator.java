package by.bsu.threads.creator;

import by.bsu.threads.action.RandomGenerator;
import by.bsu.threads.entity.Car;
import by.bsu.threads.entity.CarPark;

import java.util.LinkedList;

public class ParkCreator {
    private static final int MAX_CAR_NUMBER = 5;

    public LinkedList<Car> makeCarList(CarPark carPark) {
        RandomGenerator randomGenerator = new RandomGenerator();
        LinkedList<Car> cars = new LinkedList<>();
        for(int i =0; i < MAX_CAR_NUMBER; i++) {
            String carName = randomGenerator.thinkCarName();
            int parkTime = randomGenerator.thinkParkTime();
            int waitTime = randomGenerator.thinkWaitingTime();
            int occupiedPlaceNumber = randomGenerator.thinkOccupiedPlaceNumber();
            cars.add(new Car(carName, parkTime, waitTime, occupiedPlaceNumber, carPark));
        }
        return cars;
    }
}
