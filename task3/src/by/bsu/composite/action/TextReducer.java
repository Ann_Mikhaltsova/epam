package by.bsu.composite.action;

import by.bsu.composite.entity.Composite;

public class TextReducer {
    private StringBuilder text = new StringBuilder();
    private StringBuilder textWithFirstLetterOccurrenceDeletion = new StringBuilder();
    private boolean doFirstLetterOccurrenceDeletionTask;

    public StringBuilder getText() {
        return text;
    }

    public StringBuilder getTextWithFirstLetterOccurrenceDeletion() {
        return textWithFirstLetterOccurrenceDeletion;
    }

    public void setDoFirstLetterOccurrenceDeletionTask(boolean doFirstLetterOccurrenceDeletionTask) {
        this.doFirstLetterOccurrenceDeletionTask = doFirstLetterOccurrenceDeletionTask;
    }

    public void clearText() {
        int textBeginingIndex = 0;
        text.delete(textBeginingIndex, text.length());
    }

    public void reduceText(Composite composite, StringBuilder stringBuilder) {
        composite.appendLeafElementToString(stringBuilder, doFirstLetterOccurrenceDeletionTask);
    }
}
