package by.bsu.composite.action;

import by.bsu.composite.entity.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {
    private static final String REGEX_PARAGRAPH = "\\s{4}";
    private static final String REGEX_LISTING = "\\/\\/##.*?\\/\\/##";
    private static final String COMMENT_MARK = "//##";
    private static final String REGEX_SENTENCE = "[A-ZА-Я0-9].*?([\\.\\?!]|([:;]\\s*))";
    private static final String REGEX_WORD = "\\S+.*?[\\s\\p{Punct}]";
    private static final String REGEX_SYMBOL = ".{1}";
    private static final String REGEX_PUNCTUATION_MARK = "[\\p{Punct}]";
    private static final String SPACE = "\\s+";

    public void parseToTextAndListing(Composite composite, String string, Functional functional) {
        Pattern p = Pattern.compile(REGEX_LISTING);
        Matcher m;
        String helpString = "";
        String[] paragraphs = string.split(REGEX_PARAGRAPH);
        for(int i = 0; i < paragraphs.length; i++) {
            helpString = paragraphs[i];
            if(!helpString.isEmpty()) {
                if (isCommentBegin(helpString)) {
                    m = p.matcher(string.substring(string.indexOf(helpString),
                            string.length()));
                    if (m.find()) {
                        String tempForListing = m.group();
                        Listing listing = new Listing(tempForListing);
                        listing.setComponentType(ComponentType.LISTING);
                        composite.add(listing);
                        i = findCommentEndIndex(paragraphs, i);
                    }
                } else {
                    Composite helpComponent = parseToSentence(helpString, functional);
                    helpComponent.setComponentType(ComponentType.PARAGRAPH);
                    composite.add(helpComponent);
                }
            }
        }
    }

    private boolean isCommentBegin(String string) {
        Pattern p = Pattern.compile(COMMENT_MARK);
        Matcher m = p.matcher(string);
        return m.matches();
    }

    private int findCommentEndIndex(String[] paragraph, int startPosition) {
        int index = startPosition;
        for(int i = startPosition + 1; i < paragraph.length; i++) {
            if(paragraph[i].contains(COMMENT_MARK)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public Composite parseToSentence(String string, Functional functional) {
        Composite composite = new Composite();
        Pattern p = Pattern.compile(REGEX_SENTENCE);
        Matcher m = p.matcher(string);
        String helpString = "";
        while (m.find()) {
            helpString = m.group();
            Composite helpComposite = parseToWordAndPunctuationMark(helpString);
            helpComposite.setComponentType(ComponentType.SENTENCE);
            composite.add(helpComposite);
            functional.getOnlySentences().add(helpComposite);
        }
        return composite;
    }

    public Composite parseToWordAndPunctuationMark(String string) {
        Composite composite = new Composite();
        Pattern p = Pattern.compile(REGEX_WORD);
        Matcher m = p.matcher(string);
        String helpString = "";
        PunctuationMark punctuationMark = null;
        while (m.find()) {
            helpString = m.group();
            helpString = helpString.replaceAll(SPACE, "");
            if(isPunctuationMark(Character.toString(helpString.charAt(helpString.length() - 1)))) {
                punctuationMark = new PunctuationMark(Character.toString(helpString.charAt(helpString.length() - 1)));
                punctuationMark.setComponentType(ComponentType.PUNCTUATION_MARK);
            }
            helpString = helpString.replaceAll(REGEX_PUNCTUATION_MARK, "");
            Composite helpComposite = parseToSymbol(helpString);
            helpComposite.setComponentType(ComponentType.WORD);
            composite.setCertainChildrenNumber(composite.getCertainChildrenNumber() + 1);
            composite.add(helpComposite);
            if(punctuationMark != null) {
                composite.add(punctuationMark);
                punctuationMark = null;
            }
        }
        return composite;
    }

    private boolean isPunctuationMark(String string) {
        Pattern p = Pattern.compile(REGEX_PUNCTUATION_MARK);
        Matcher m = p.matcher(string);
        return m.matches();
    }

    public Composite parseToSymbol(String string) {
        Composite composite = new Composite();
        Pattern p = Pattern.compile(REGEX_SYMBOL);
        Matcher m = p.matcher(string);
        String helpString = "";
        Symbol symbol = null;
        int startSymbolIndex = 0;
        while (m.find()) {
            helpString = m.group();
            symbol = new Symbol(helpString.charAt(startSymbolIndex));
            symbol.setComponentType(ComponentType.SYMBOL);
            composite.add(symbol);
        }
        return composite;
    }
}
