package by.bsu.composite.action;

import java.util.Iterator;
import java.util.List;

public class TextAppender {
    public String makeTextFromStrings(List<String> textStrings) {
        String wholeTextInString = "";
        Iterator<String> iterator = textStrings.iterator();
        while(iterator.hasNext()) {
            wholeTextInString += iterator.next();
        }
        return wholeTextInString;
    }
}
