package by.bsu.composite.action;

import by.bsu.composite.comparator.SentencesByWordNumberComparator;
import by.bsu.composite.entity.Composite;

import java.util.Collections;

public class Functional {
    private Composite onlySentences = new Composite();

    public Composite getOnlySentences() {
        return onlySentences;
    }

    public void groupSentencesByWordNumber () {
        Collections.sort(onlySentences.getComponents(),
                SentencesByWordNumberComparator.getSentencesByWordNumberComparator());
    }
}
