package by.bsu.composite.report;

public class Report {
    public void printSourceText(String string) {
        System.out.println("Source text :");
        System.out.println(string);
    }

    public void printReducedText(StringBuilder stringBuilder) {
        System.out.println("Reduced text :");
        System.out.println(stringBuilder);
    }

    public void printSentencesSortedByWordNumber(StringBuilder stringBuilder) {
        System.out.println("All sentences of the text in increase of words in sentences order : ");
        System.out.println(stringBuilder);
    }

    public void printTextWithFirstLetterOccurrenceDeletion(StringBuilder stringBuilder) {
        System.out.println("Text, in which occurrences of first letter of word are deleted : ");
        System.out.println(stringBuilder);
    }
}
