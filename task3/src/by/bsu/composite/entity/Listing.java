package by.bsu.composite.entity;

public class Listing implements Component {
    private String listing;
    private ComponentType componentType;

    public Listing(String listing) {
        this.listing = listing;
    }

    @Override
    public int getCertainChildrenNumber() {
        return 0;
    }

    @Override
    public ComponentType getComponentType() {
        return componentType;
    }

    @Override
    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    @Override
    public void appendLeafElementToString(StringBuilder stringBuilder, boolean doFirstLetterOccurrenceDeletionTask) {
        stringBuilder.append(listing + " ");
    }

    @Override
    public Object takeLeafElement(int index) {
        return listing;
    }
}
