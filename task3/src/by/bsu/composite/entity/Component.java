package by.bsu.composite.entity;


public interface Component {
    int getCertainChildrenNumber();
    ComponentType getComponentType();
    void setComponentType(ComponentType componentType);
    void appendLeafElementToString(StringBuilder stringBuilder, boolean doFirstLetterOccurrenceDeletionTask);
    Object takeLeafElement(int index);
}
