package by.bsu.composite.entity;

public class Symbol implements Component {
    private char symbol;
    private ComponentType componentType;

    public Symbol(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    @Override
    public int getCertainChildrenNumber() {
        return 0;
    }

    @Override
    public ComponentType getComponentType() {
        return componentType;
    }

    @Override
    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    @Override
    public void appendLeafElementToString(StringBuilder stringBuilder, boolean doFirstLetterOccurrenceDeletionTask) {
        stringBuilder.append(symbol);
    }

    @Override
    public Object takeLeafElement(int index) {
        return symbol;
    }

    @Override
    public String toString() {
        return Character.toString(symbol);
    }
}
