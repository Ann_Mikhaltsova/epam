package by.bsu.composite.entity;

public enum ComponentType {
    SOURCE_TEXT_FILE,
    LISTING,
    PARAGRAPH,
    SENTENCE,
    WORD,
    SYMBOL,
    PUNCTUATION_MARK
}
