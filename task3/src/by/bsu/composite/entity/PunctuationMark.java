package by.bsu.composite.entity;

public class PunctuationMark implements Component {
    private String punctuationMark;
    private ComponentType componentType;

    public PunctuationMark(String punctuationMark) {
        this.punctuationMark = punctuationMark;
    }

    @Override
    public int getCertainChildrenNumber() {
        return 0;
    }

    @Override
    public ComponentType getComponentType() {
        return componentType;
    }

    @Override
    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    @Override
    public void appendLeafElementToString(StringBuilder stringBuilder, boolean doFirstLetterOccurrenceDeletionTask) {
        stringBuilder.append(punctuationMark + " ");
    }

    @Override
    public Object takeLeafElement(int index) {
        return punctuationMark;
    }
}
