package by.bsu.composite.entity;

import org.apache.log4j.Logger;

import java.util.ArrayList;

public class Composite implements Component {
    private static final Logger LOG = Logger.getLogger(Composite.class);
    private ArrayList<Component> components = new ArrayList<>();
    private ComponentType componentType;
    private int certainChildrenNumber = 0;

    public ArrayList<Component> getComponents() {
        return components;
    }

    public void setCertainChildrenNumber(int certainChildrenNumber) {
        this.certainChildrenNumber = certainChildrenNumber;
    }

    public void add(Component component) {
        components.add(component);
    }

    public void remove(Component component) {
        components.remove(component);
    }

    @Override
    public int getCertainChildrenNumber() {
        return certainChildrenNumber;
    }

    @Override
    public ComponentType getComponentType() {
        return componentType;
    }

    @Override
    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    @Override
    public void appendLeafElementToString(StringBuilder stringBuilder, boolean doFirstLetterOccurrenceDeletionTask) {
        int firstLetterBeginingIndex = 0;
        int startSymbolIndex = 0;
        int indexForReachingSpaceBeforePunctuationMark = 3;
        for(int i = 0; i < components.size(); i++) {
            if(doFirstLetterOccurrenceDeletionTask && components.get(i).getComponentType() == ComponentType.WORD) {
                Composite word = (Composite)getComponents().get(i);
                LOG.debug("Word - " + word);
                Symbol firstLetter = (Symbol)word.takeLeafElement(firstLetterBeginingIndex);
                LOG.debug("First letter - " + firstLetter);
                for(int j = 1; j < word.getComponents().size(); j++) {
                    Symbol helpSymbol = (Symbol)word.takeLeafElement(j);
                    LOG.debug("Symbol - " + helpSymbol);
                    if(Character.toString(firstLetter.getSymbol()).toLowerCase().charAt(startSymbolIndex) ==
                            (helpSymbol.getSymbol())) {
                        Component componentForDeletion = (Component) (getComponents().get(i)).takeLeafElement(j);
                        ((Composite) getComponents().get(i)).
                                remove(componentForDeletion);
                        j--;
                    }
                }
            }
            components.get(i).appendLeafElementToString(stringBuilder, doFirstLetterOccurrenceDeletionTask);
            if(components.get(i).getComponentType() == ComponentType.WORD) {
                stringBuilder.append(" ");
            }
            if(components.get(i).getComponentType() == ComponentType.PUNCTUATION_MARK) {
                stringBuilder.deleteCharAt(stringBuilder.length() - indexForReachingSpaceBeforePunctuationMark);
            }
        }
    }

    @Override
    public Object takeLeafElement(int index) {
        return getComponents().get(index);
    }

    @Override
    public String toString() {
        String helpString = "";
        for(Component component : components) {
            helpString += component.toString();
        }
        return helpString;
    }
}
