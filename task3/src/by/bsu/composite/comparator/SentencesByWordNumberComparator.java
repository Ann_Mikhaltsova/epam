package by.bsu.composite.comparator;

import by.bsu.composite.entity.Component;

import java.util.Comparator;

public class SentencesByWordNumberComparator {
    public static Comparator<Component> getSentencesByWordNumberComparator() {
        return new Comparator<Component>() {
            @Override
            public int compare(Component c1, Component c2) {
                return c1.getCertainChildrenNumber() - c2.getCertainChildrenNumber();
            }
        };
    }
}
