package by.bsu.composite.input;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public interface FileHandler {
    static List<String> linesList(String firstPath, String... morePath)  {
        Path path = FileSystems.getDefault().getPath(firstPath, morePath);
        Stream<String> lines = null;
        try {
            lines = Files.lines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String> listNames = new ArrayList<>();
        if (lines != null) {
            lines.forEach(fioTmp -> listNames.add(fioTmp));
        }
        return listNames;
    }
}
