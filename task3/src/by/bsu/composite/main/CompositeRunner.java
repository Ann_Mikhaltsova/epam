package by.bsu.composite.main;

import by.bsu.composite.action.Functional;
import by.bsu.composite.action.TextAppender;
import by.bsu.composite.action.TextParser;
import by.bsu.composite.action.TextReducer;
import by.bsu.composite.entity.ComponentType;
import by.bsu.composite.entity.Composite;
import by.bsu.composite.input.FileHandler;
import by.bsu.composite.report.Report;
import org.apache.log4j.Logger;

import java.util.List;

public class CompositeRunner {
    private static final Logger LOG = Logger.getLogger(CompositeRunner.class);

    public static void main(String[] args) {
        LOG.info("Reading from file");
        List<String> textStrings = FileHandler.linesList("in", "in.txt");
        String wholeTextInString = new TextAppender().makeTextFromStrings(textStrings);
        Report report = new Report();
        LOG.info("Print all text");
        report.printSourceText(wholeTextInString);
        Composite composite = new Composite();
        composite.setComponentType(ComponentType.SOURCE_TEXT_FILE);
        TextParser textParser = new TextParser();
        Functional functional = new Functional();
        LOG.info("Parsing");
        textParser.parseToTextAndListing(composite, wholeTextInString, functional);
        TextReducer textReducer = new TextReducer();
        LOG.info("Text reducing");
        textReducer.reduceText(composite, textReducer.getText());
        LOG.info("Print reduced text");
        report.printReducedText(textReducer.getText());
        LOG.info("Sorting");
        functional.groupSentencesByWordNumber();
        textReducer.clearText();
        textReducer.reduceText(functional.getOnlySentences(), textReducer.getText());
        LOG.info("Print sorted text");
        report.printSentencesSortedByWordNumber(textReducer.getText());
        LOG.info("Delete occurrence of the first letter of each word in each sentence");
        textReducer.setDoFirstLetterOccurrenceDeletionTask(true);
        textReducer.reduceText(composite, textReducer.getTextWithFirstLetterOccurrenceDeletion());
        report.printTextWithFirstLetterOccurrenceDeletion(textReducer.getTextWithFirstLetterOccurrenceDeletion());
    }
}


