<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />

<html>
<head>
    <title><fmt:message key="client.showbill" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/client.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/form.css" type="text/css">

    <script type="text/javascript" src="${path}/js/scrollup.js"></script>
</head>
<body>
<div id="wrapper">
    <header>
        <div id="logotype">
            <a href="index.jsp">
                <img src="${path}/pictures/logo.png" alt="" height=100 />
            </a>
        </div>
        <div id="topBar">
            <ul>
                <li><a href="/jsp/client.jsp"><fmt:message key="client.home" /></a></li>
                <ul style="float:right;list-style-type:none;">
                    <li>
                        <form action="main" method="post">
                            <input type="hidden" name="command" value="logout" />
                            <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                        </form>
                    </li>
                </ul>
            </ul>
        </div>
    </header>
    <section class="clientBill">
        <c:if test="${not empty errorMessage }">
            <div id="danger">${errorMessage}</div>
        </c:if>
        <c:if test="${not empty actionMessage }">
            <div id="success">${actionMessage }</div>
        </c:if>
        <div id="bill">
            <fieldset>
                <legend><fmt:message key="client.showbill" /></legend>
                <c:if test="${not empty emptyBill }">
                    <c:out value="${emptyBill}" />
                </c:if>
                <c:if test="${empty emptyBill }">
                    <table id="clientBill">
                        <tr>
                            <td><fmt:message key="clientorder.datein" /></td>
                            <td><fmt:message key="clientorder.dateout" /></td>
                            <td><fmt:message key="client.form.roomnumber" /></td>
                            <td><fmt:message key="clientbill.totalcost" /></td>
                            <td><fmt:message key="clientorders.orderstatus" /></td>
                            <td><fmt:message key="clientbill.pay" /></td>
                        </tr>
                        <c:forEach var="bill" items="${clientBill}">
                            <tr>
                                <td><c:out value="${bill.order.dateIn}" /></td>
                                <td><c:out value="${bill.order.dateOut}" /></td>
                                <td><c:out value="${bill.order.room.roomNumber}" /></td>
                                <td><c:out value="${bill.totalCost}" /></td>
                                <td><c:out value="${bill.billStatus}" /></td>
                                <td>
                                    <form action="main" method="post">
                                        <input type="hidden" name="command" value="payment" />
                                        <input type="hidden" name="idBill" value="${bill.id }" />
                                        <button id="payment" type="submit">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </fieldset>
        </div>
    </section>
</div>
</body>
</html>
