<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />
<html>
<head>
    <title><fmt:message key="registration.title" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/login.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/footer.css" type="text/css">
    <script type="text/javascript" src="${path}/js/scrollup.js"></script>
</head>
<body>
<div id="wrapper">
    <header>
        <div id="logotype">
            <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
        </div>
        <div id="topBar">
            <ul>
                <li><a href="javascript:history.back()"><fmt:message key="client.back" /></a></li>
                <ul style="float:right;list-style-type:none;">
                    <li>
                        <form action="main" method="post">
                            <input type="hidden" name="command" value="logout" />
                            <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                        </form>
                    </li>
                </ul>
            </ul>
        </div>
    </header>
    <c:if test="${not empty errorMessage }">
        <div id="danger">${errorMessage}</div>
    </c:if>
    <section class="loginform">
        <div id="section">
            <form action="main" method="post">
                <fieldset>
                    <legend><fmt:message key="registration.title" /></legend>

                    <label for="login"><span><span style="color:red">*</span>
                        <fmt:message key="signing.login" /></span></label>
                    <input id="login" type="text" autocomplete="off" required autofocus name="login"
                           pattern="^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z/\d]{4,19}$"
                           placeholder="<fmt:message key="signing.login" />"
                           title="<fmt:message key="registration.login-validation-help" />"/>

                    <label for="password"><span><span style="color:red">*</span>
                        <fmt:message key="signing.password" /></span></label>
                    <input id="password" type="password" autocomplete="off" required name="password"
                           pattern="^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z0-9-_\.]{4,19}$"
                           placeholder="<fmt:message key="signing.password" />"
                           title="<fmt:message key="registration.password-validation-help" />"/>

                    <label for="passwordagain"><span><span style="color:red">*</span>
                        <fmt:message key="registration.passwordagain" /></span></label>
                    <input id="passwordagain" type="password" autocomplete="off" required name="passwordagain"
                           pattern="^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z0-9-_\.]{4,19}$"
                           placeholder="<fmt:message key="registration.passwordagain" />"
                           title="<fmt:message key="registration.passwordagain-validation-help" />"/>

                    <label for="email"><span><span style="color:red">*</span>
                        <fmt:message key="registration.email" /></span></label>
                    <input id="email" type="email" autocomplete="off" required name="email"
                           pattern="^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$"
                           placeholder="<fmt:message key="registration.email" />"
                           title="<fmt:message key="registration.email-validation-help" />"/>

                    <label for="name"><span><span style="color:red">*</span>
                        <fmt:message key="registration.name" /></span></label>
                    <input id="name" type="text" autocomplete="off" required name="name"
                           pattern="^[A-ZА-ЯЁ][a-zа-яё]{2,19}$"
                           placeholder="<fmt:message key="registration.name" />"
                           title="<fmt:message key="registration.name-validation-help" />"/>

                    <label for="surname"><span><span style="color:red">*</span>
                        <fmt:message key="registration.surname" /></span></label>
                    <input id="surname" type="text" autocomplete="off" required name="surname"
                           pattern="^[A-ZА-ЯЁ][a-zа-яё]{4,19}$"
                           placeholder="<fmt:message key="registration.surname" />"
                           title="<fmt:message key="registration.surname-validation-help" />"/>

                    <label for="patronymic"><span><span style="color:red">*</span>
                        <fmt:message key="registration.patronymic" /></span></label>
                    <input id="patronymic" type="text" autocomplete="off" required name="patronymic"
                           pattern="^[A-ZА-ЯЁ][a-zа-яё]{4,19}$"
                           placeholder="<fmt:message key="registration.patronymic" />"
                           title="<fmt:message key="registration.patronymic-validation-help" />"/>

                    <label for="birthday"><span><span style="color:red">*</span>
                        <fmt:message key="registration.birthday" /></span></label>
                    <input id="birthday" type="date" required name="birthday"
                           placeholder="<fmt:message key="registration.birthday" />" />

                    <input type="hidden" name="command" value="registration" />
                    <input type="submit" name="registration"
                           value="<fmt:message key="signing.registration" />">
                </fieldset>
            </form>
        </div>
    </section>
</div>
<footer>
    <div id="footer">
        <div id="scrollup"><img alt="<fmt:message key="index.scrollup.alt" />" src="${path}/pictures/up.png"></div>
        <div id="authorInfo">
            <p><fmt:message key="index.footer" /></p>
        </div>
    </div>
</footer>
</body>
</html>




