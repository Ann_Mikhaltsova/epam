<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="htl" uri="/WEB-INF/tld/custom.tld" %>

<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />
<html>
<head>
    <title><fmt:message key="client.title" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/client.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/footer.css" type="text/css">

    <script type="text/javascript" src="${path}/js/scrollup.js"></script>
</head>
<body>
<div id="wrapper">
    <header>
        <div id="logotype">
            <a href="index.jsp">
                <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
            </a>
        </div>
        <div id="topBar">
            <ul>
                <li><a href="javascript:history.back()"><fmt:message key="client.back" /></a></li>
                <ul style="float:right;list-style-type:none;">
                    <li><a class="active" href="#about"><fmt:message key="client.about" /></a></li>
                    <li>
                        <form action="main" method="post">
                            <input type="hidden" name="command" value="logout" />
                            <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                        </form>
                    </li>
                </ul>
            </ul>
        </div>
    </header>
    <section>
        <div id="controlButton">
            <form action="main" method="post">
                <input type="hidden" name="command" value="roomAdministration" />
                <input type="submit" value="<fmt:message key="admin.rooms" />">
            </form>
            <form action="main" method="post">
                <input type="hidden" name="command" value="orderAdministration" />
                <input type="submit" value="<fmt:message key="admin.orders" />">
            </form>
        </div>
        <c:if test="${not empty errorMessage }">
            <div id="danger">${errorMessage}</div>
        </c:if>
        <c:if test="${not empty actionMessage }">
            <div id="success">${actionMessage }</div>
        </c:if>
        <table id="round">
            <thead>
            <tr>
                <th colspan="2"><fmt:message key="admin.table-title" /></th>
            </tr>
            </thead>
            <tr>
                <td><fmt:message key="signing.login" /></td>
                <td>${user.login}</td>
            </tr>
            <tr>
                <td><fmt:message key="registration.name" /></td>
                <td>${user.name}</td>
            </tr>
            <tr>
                <td><fmt:message key="registration.surname" /></td>
                <td>${user.surname}</td>
            </tr>
            <tr>
                <td><fmt:message key="registration.patronymic" /></td>
                <td>${user.patronymic}</td>
            </tr>
            <tr>
                <td><fmt:message key="registration.email" /></td>
                <td>${user.email}</td>
            </tr>
            <tr>
                <td><fmt:message key="registration.birthday" /></td>
                <td>${user.birthday}</td>
            </tr>
        </table>
        <div id="today">
            <htl:today format="dd MMMM, yyyy" />
        </div>
    </section>
    <div id="footer">
        <div id="scrollup"><img alt="<fmt:message key="index.scrollup.alt" />" src="${path}/pictures/up.png"></div>
        <div id="authorInfo">
            <p><fmt:message key="index.footer" /></p>
        </div>
    </div>
</div>
</body>
</html>