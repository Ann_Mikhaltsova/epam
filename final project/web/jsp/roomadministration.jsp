<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />

<html>
<head>
    <title><fmt:message key="roomadministration.allrooms" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/client.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/form.css" type="text/css">

    <script type="text/javascript" src="${path}/js/scrollup.js"></script>
</head>
<body>
<div id="wrapper">
    <header>
        <div id="logotype">
            <a href="index.jsp">
                <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
            </a>
        </div>
        <div id="topBar">
            <ul>
                <li><a href="/jsp/admin.jsp"><fmt:message key="client.home" /></a></li>
                <ul style="float:right;list-style-type:none;">
                    <li>
                        <form action="main" method="post">
                            <input type="hidden" name="command" value="logout" />
                            <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                        </form>
                    </li>
                </ul>
            </ul>
        </div>
    </header>
    <section class="createOrder">
        <c:if test="${not empty errorMessage }">
            <div id="danger">${errorMessage}</div>
        </c:if>
        <c:if test="${not empty actionMessage }">
            <div id="success">${actionMessage }</div>
        </c:if>
        <div id="order">
            <fieldset>
                <legend><fmt:message key="roomadministration.allrooms" /></legend>
                    <form action="main" method="post">
                        <input type="hidden" name="command" value="page">
                        <input type="hidden" name="page" value="addroom">
                        <input type="submit" value="<fmt:message key="roomadministration.addroom" />">
                    </form>
                <table id="freeRooms">
                    <thead>
                    <tr>
                        <th><fmt:message key="roomadministration.allrooms" /></th>
                    </tr>
                    </thead>
                    <tr>
                        <td><fmt:message key="client.form.roomnumber" /></td>
                        <td><fmt:message key="client.form.nplace" /></td>
                        <td><fmt:message key="client.form.roomcategory" /></td>
                        <td><fmt:message key="client.form.cost" /></td>
                        <td><fmt:message key="clientorders.orderstatus" /></td>
                        <td><fmt:message key="roomadministration.changeroomstatus" /></td>
                    </tr>
                    <c:forEach var="room" items="${roomList}">
                        <tr>
                            <td>
                                <c:out value="${room.roomNumber}" />
                                <form action="main" method="post">
                                    <input id="roomNumber" type="text" autocomplete="off" required autofocus name="roomNumber"
                                       pattern="^[0-9]{2,}$"
                                       title="<fmt:message key="roomadministration.roomnumber-validation-help" />"/>
                                    <input type="hidden" name="command" value="changeRoomNumber" />
                                    <input type="hidden" name="idRoom" value="${room.id }" />
                                    <input type="submit" value="<fmt:message key="roomadministration.changestatus" />" />
                                </form>
                            </td>
                            <td><c:out value="${room.placeForSleep}" /></td>
                            <td><c:out value="${room.category }" /></td>
                            <td><c:out value="${room.costPerDay}" /></td>
                            <td><c:out value="${room.roomStatus}" /></td>
                            <td>
                                <form action="main" method="post">
                                    <select name="roomStatus">
                                        <option value="empty">
                                            <fmt:message key="roomadministration.empty" />
                                        </option>
                                        <option value="FREE">
                                            <fmt:message key="roomadministration.free" />
                                        </option>
                                        <option value="BUSY">
                                            <fmt:message key="roomadministration.busy" />
                                        </option>
                                        <option value="REPAIR">
                                            <fmt:message key="client.form.onrepair" />
                                        </option>
                                    </select>
                                    <input type="hidden" name="command" value="setStatusRoom" />
                                    <input type="hidden" name="idRoom" value="${room.id }" />
                                    <input type="submit" value="<fmt:message key="roomadministration.changestatus" />" />
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </fieldset>
        </div>
    </section>
</div>
</body>
</html>
