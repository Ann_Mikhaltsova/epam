<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />

<html>
<head>
    <title><fmt:message key="client.form.makeorder" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/client.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/form.css" type="text/css">

    <script type="text/javascript" src="${path}/js/scrollup.js"></script>
</head>
<body>
<div id="wrapper">
    <header>
        <div id="logotype">
            <a href="index.jsp">
                <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
            </a>
        </div>
        <div id="topBar">
            <ul>
                <li><a href="/jsp/client.jsp"><fmt:message key="client.home" /></a></li>
                <ul style="float:right;list-style-type:none;">
                    <li>
                        <form action="main" method="post">
                            <input type="hidden" name="command" value="logout" />
                            <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                        </form>
                    </li>
                </ul>
            </ul>
        </div>
    </header>
    <section class="createOrder">
        <c:if test="${not empty errorMessage }">
            <div id="danger">${errorMessage}</div>
        </c:if>
        <c:if test="${not empty actionMessage }">
            <div id="success">${actionMessage }</div>
        </c:if>
        <div id="order">
            <form>
                <fieldset>
                    <legend><fmt:message key="client.form.makeorder" /></legend>

                    <h2 id="dateInfo" class="title"><fmt:message key="client.form.dateinfo" /></h2>
                    <div id="chooseDate">
                        <label for="dateIn"><span><fmt:message key="client.form.datein" /></span></label>
                        <input class="date" id="dateIn" type="date" required name="dateIn"
                               placeholder="" />
                        <label for="dateOut"><span><fmt:message key="client.form.dateout" /></span></label>
                        <input class="date" id="dateOut" type="date" required name="dateOut"
                               placeholder="" />
                    </div>

                    <table id="freeRooms">
                        <c:set var="repair" value="REPAIR" />
                        <thead>
                        <tr>
                            <th><fmt:message key="client.form.free-room-info" /></th>
                        </tr>
                        </thead>
                        <tr>
                            <td><fmt:message key="client.form.roomnumber" /></td>
                            <td><fmt:message key="client.form.nplace" /></td>
                            <td><fmt:message key="client.form.roomcategory" /></td>
                            <td><fmt:message key="client.form.cost" /></td>
                            <td><fmt:message key="client.form.choose" /></td>
                        </tr>
                        <c:forEach var="room" items="${freeRoom}">
                            <tr>
                                <td><c:out value="${room.roomNumber}" /></td>
                                <td><c:out value="${room.placeForSleep}" /></td>
                                <td><c:out value="${room.category }" /></td>
                                <td><c:out value="${room.costPerDay}" /></td>
                                <c:if test="${room.roomStatus eq repair}">
                                    <td><fmt:message key="client.form.onrepair" /></td>
                                </c:if>
                                <c:if test="${room.roomStatus ne repair}">
                                    <td><input type="radio" name="idRoom" required value="${room.id}" /></td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </table>

                    <input type="hidden" name="command" value="createOrder" />
                    <input id="createOrder" type="submit" value="<fmt:message key="client.form.makeorder" />" />
                </fieldset>
            </form>
        </div>
    </section>
</div>
</body>
</html>