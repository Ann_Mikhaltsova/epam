<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="htl" uri="/WEB-INF/tld/custom.tld" %>

<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />

<html>
<head>
    <title><fmt:message key="orderadministration.allorders" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/client.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/form.css" type="text/css">

    <script type="text/javascript" src="${path}/js/scrollup.js"></script>
</head>
<body>
<div id="wrapper">
    <header>
        <div id="logotype">
            <a href="index.jsp">
                <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
            </a>
        </div>
        <div id="topBar">
            <ul>
                <li><a href="/jsp/admin.jsp"><fmt:message key="client.home" /></a></li>
                <ul style="float:right;list-style-type:none;">
                    <li>
                        <form action="main" method="post">
                            <input type="hidden" name="command" value="logout" />
                            <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                        </form>
                    </li>
                </ul>
            </ul>
        </div>
    </header>
    <section class="clientOrder">
        <c:if test="${not empty errorMessage }">
            <div id="danger">${errorMessage}</div>
        </c:if>
        <c:if test="${not empty actionMessage }">
            <div id="success">${actionMessage }</div>
        </c:if>
        <div id="order">
            <fieldset>
                <legend><fmt:message key="orderadministration.allorders" /></legend>
                <c:if test="${not empty emptyOrder }">
                    <c:out value="${emptyOrder}" />
                </c:if>
                <c:if test="${empty emptyOrder }">
                    <table id="clientOrder">
                        <tr>
                            <td><fmt:message key="orderadministration.iduser" /></td>
                            <td><fmt:message key="client.form.roomnumber" /></td>
                            <td><fmt:message key="clientorder.datein" /></td>
                            <td><fmt:message key="clientorder.dateout" /></td>
                            <td><fmt:message key="client.form.cost" /></td>
                            <td><fmt:message key="clientorders.orderstatus" /></td>
                            <td><fmt:message key="roomadministration.changeroomstatus" /></td>
                        </tr>
                        <c:forEach var="order" items="${orderList}">
                            <tr>
                                <htl:days dateOut="${order.dateOut}" dateIn="${order.dateIn}" />
                                <td><c:out value="${order.user.id}" /></td>
                                <td><c:out value="${order.room.roomNumber}" /></td>
                                <td><c:out value="${order.dateIn}" /></td>
                                <td><c:out value="${order.dateOut}" /></td>
                                <td><c:out value="${order.room.costPerDay}" /></td>
                                <td><c:out value="${order.orderStatus}" /></td>
                                <td>
                                    <form action="main" method="post">
                                        <select name="orderStatus">
                                            <option value="empty">
                                                <fmt:message key="roomadministration.empty" />
                                            </option>
                                            <option value="CONFIRMED">
                                                <fmt:message key="orderadministration.confirmed" />
                                            </option>
                                            <option value="PENDING">
                                                <fmt:message key="orderadministration.pending" />
                                            </option>
                                            <option value="DECLINED">
                                                <fmt:message key="orderadministration.declined" />
                                            </option>
                                        </select>
                                        <input type="hidden" name="command" value="setStatusOrder" />
                                        <input type="hidden" name="idOrder" value="${order.id }" />
                                        <input type="submit" value="<fmt:message key="roomadministration.changestatus" />" />
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </fieldset>
        </div>
    </section>
</div>
</body>
</html>
