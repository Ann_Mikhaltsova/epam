<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />
<html>
<head>
    <title><fmt:message key="signing.title" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/login.css" type="text/css">
</head>
<body>
    <div id="wrapper">
        <header>
            <div id="logotype">
                <a href="index.jsp">
                    <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
                </a>
            </div>
            <div id="topBar">
                <ul>
                    <li><a href="javascript:history.back()"><fmt:message key="client.back" /></a></li>
                    <ul style="float:right;list-style-type:none;">
                        <li>
                            <form action="main" method="post">
                                <input type="hidden" name="command" value="logout" />
                                <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                            </form>
                        </li>
                    </ul>
                </ul>
            </div>
        </header>
        <c:if test="${not empty errorMessage }">
            <div id="danger">${errorMessage}</div>
        </c:if>
        <section class="loginform">
            <div id="section">
                <form method="post" action="main">
                    <fieldset>
                        <legend><fmt:message key="signing.title" /></legend>

                        <input type="hidden" name="command" value="login" />
                        <label for="login"><fmt:message key="signing.login" /></label>
                        <input id="login" type="text" autocomplete="off" required autofocus name="login"
                               pattern="^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z/\d]{4,19}$"
                               placeholder="<fmt:message key="signing.login" />" />

                        <label for="password"><fmt:message key="signing.password" /></label>
                        <input id="password" type="password" autocomplete="off" required name="password"
                               pattern="^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z0-9-_\.]{4,19}$"
                               placeholder="<fmt:message key="signing.password" />" />
                        <input type="submit" name="return" value="<fmt:message key="signing.signin" />">
                    </fieldset>
                </form>
            </div>
        </section>
    </div>
    <div id="registration">
        <p><fmt:message key="signing.registration-reminder" /></p>
        <form action="main" method="post">
            <input type="hidden" name="command" value="page" />
            <input type="hidden" name="page" value="registration">
            <input type="submit" name="login" value="<fmt:message key="signing.registration" />">
        </form>
    </div>
</body>
</html>
