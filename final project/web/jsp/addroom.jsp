<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${lang }" />

<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />
<html>
    <head>
        <title><fmt:message key="roomadministration.addroom" /></title>
        <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
        <LINK rel="stylesheet" href="${path}/css/client.css" type="text/css">
        <LINK rel="stylesheet" href="${path}/css/form.css" type="text/css">
    </head>
    <body>
    <div id="wrapper">
        <header>
            <div id="logotype">
                <a href="index.jsp">
                    <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
                </a>
            </div>
            <div id="topBar">
                <ul>
                    <li><a href="/jsp/admin.jsp"><fmt:message key="client.back" /></a></li>
                    <ul style="float:right;list-style-type:none;">
                        <li>
                            <form action="main" method="post">
                                <input type="hidden" name="command" value="logout" />
                                <input id="logout" type="submit" value="<fmt:message key="client.logout" />">
                            </form>
                        </li>
                    </ul>
                </ul>
            </div>
        </header>
        <c:if test="${not empty errorMessage }">
            <div id="danger">${errorMessage}</div>
        </c:if>
        <c:if test="${not empty actionMessage }">
            <div id="success">${actionMessage }</div>
        </c:if>
        <section class="loginform">
            <div id="section">
                <form action="main" method="post">
                    <fieldset>
                        <legend><fmt:message key="roomadministration.addroom" /></legend>

                        <label for="setRoomNumber"><span><fmt:message key="client.form.roomnumber" /></span></label>
                        <input id="setRoomNumber" type="text" autocomplete="off" required name="roomNumber"
                               pattern="^[0-9]{2,}$"
                               title="<fmt:message key="roomadministration.roomnumber-validation-help" />"/>
                        <label for="placeForSleep"><span><fmt:message key="client.form.nplace" /></span></label>
                        <input id="placeForSleep" type="text" autocomplete="off" required name="placeForSleep"
                               pattern="^[0-9]+$"
                               placeholder="<fmt:message key="client.form.nplace" />"
                               title="<fmt:message key="client.form.nplace-validation-help" />"/>

                        <select id="category" size="1" name="category" required>
                            <option selected disabled><fmt:message key="client.form.selecttitle" /></option>
                            <option value="ECONOMY"><fmt:message key="client.form.option-1" /></option>
                            <option value="STANDARD"><fmt:message key="client.form.option-2" /></option>
                            <option value="LUX"><fmt:message key="client.form.option-3" /></option>
                        </select>
                        <label for="costPerDay"><span><fmt:message key="client.form.cost" /></span></label>
                        <input id="costPerDay" type="text" autocomplete="off" required name="costPerDay"
                               pattern="^[0-9]{2,}$"
                               title="<fmt:message key="roomadministration.costperday-validation-help" />"/>

                        <input type="hidden" name="command" value="addRoom" />
                        <input id="createOrder" type="submit" value="<fmt:message key="roomadministration.addroom" />" />
                    </fieldset>
                </form>
            </div>
        </section>

    </div>
    </body>
</html>