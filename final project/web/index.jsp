<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang }" />
<fmt:setBundle basename="languages.pagelanguage" />
<html>
  <head>
    <title><fmt:message key="index.title" /></title>
    <LINK rel="stylesheet" href="${path}/css/header.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/startpage.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/css/footer.css" type="text/css">
    <LINK rel="stylesheet" href="${path}/bootstrap/css/bootstrap.min.css" type="text/css">

    <script type='text/javascript' src="${path}/js/scrollup.js"></script>
  </head>
  <body>
  <div id="wrapper">
    <header>
      <div id="logotype">
        <a href="index.jsp">
          <img src="${path}/pictures/logo.png" alt="<fmt:message key="index.hotellogo.alt" />" height=100 />
        </a>
      </div>
      <div id="rightBar">
        <form action="main" method="post">
          <input type="hidden" name="command" value="page">
          <input type="hidden" name="page" value="login">
          <input type="submit" name="login" value="<fmt:message key="index.signin" />">
        </form>
        <form action="main" method="post">
          <input type="hidden" name="command" value="language">
          <input type="hidden" name="lang" value="en_EN">
          <input class="language" id="eng" type="submit" name="eng" value="">
        </form>
        <form action="main" method="post">
          <input type="hidden" name="command" value="language">
          <input type="hidden" name="lang" value="ru_RU">
          <input class="language" id="ru" type="submit" name="rus" value="">
        </form>
      </div>
    </header>
    <section>
      <c:if test="${not empty errorMessage }">
        <div div="danger">${errorMessage}</div>
      </c:if>
      <c:if test="${not empty actionMessage }">
        <div id="success">${actionMessage }</div>
      </c:if>

      <h1>
        <fmt:message key="index.hotelname" />
      </h1>
      <p align="justify"><fmt:message key="index.info1" /></p>
      <p align="justify"><fmt:message key="index.info2" /></p>
      <div class="carousel slide" id="myCarousel">
        <ol class="carousel-indicators">
          <li class="active" data-target="#myCarousel" data-slide-to="0"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <img src="${path}/pictures/1.jpg">
            <div class="carousel-caption">
              <h3>
                <fmt:message key="index.slide-title-1" />
              </h3>
              <p>
                <fmt:message key="index.slide-body-1" />
              </p>
            </div>
          </div>
          <div class="item">
            <img src="${path}/pictures/2.jpg">
            <div class="carousel-caption">
              <h3>
                <fmt:message key="index.slide-title-2" />
              </h3>
              <p>
                <fmt:message key="index.slide-body-2" />
              </p>
            </div>
          </div>
          <div class="item">
            <img src="${path}/pictures/3.jpg">
            <div class="carousel-caption">
              <h3>
                <fmt:message key="index.slide-title-3" />
              </h3>
              <p>
                <fmt:message key="index.slide-body-3" />
              </p>
            </div>
          </div>
          <div class="item">
            <img src="${path}/pictures/4.jpg">
            <div class="carousel-caption">
              <h3>
                <fmt:message key="index.slide-title-4" />
              </h3>
              <p>
                <fmt:message key="index.slide-body-4" />
              </p>
            </div>
          </div>
        </div>
        <a class="carousel-control left" data-slide="prev" href="#myCarousel">&lsaquo;</a>
        <a class="carousel-control right" data-slide="next" href="#myCarousel">&rsaquo;</a>
      </div>
    </section>
  </div>
  <footer>
    <div id="footer">
      <div id="scrollup"><img alt="<fmt:message key="index.scrollup.alt" />" src="${path}/pictures/up.png"></div>
      <div id="authorInfo">
        <p><fmt:message key="index.footer" /></p>
      </div>
    </div>
  </footer>
  <script type="text/javascript" src="${path}/js/jquery-1.11.0.js"></script>
  <script type="text/javascript" src="${path}/js/bootstrap.min.js"></script>
  </body>
</html>
