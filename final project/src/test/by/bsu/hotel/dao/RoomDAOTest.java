package test.by.bsu.hotel.dao;

import by.bsu.hotel.dao.IRoomDAO;
import by.bsu.hotel.dao.RoomDAO;
import by.bsu.hotel.entity.Category;
import by.bsu.hotel.entity.Room;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.TechnicalException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

public class RoomDAOTest {
    private static Random random;
    private IRoomDAO roomDAO;
    private long id;
    private int roomNumber;
    private int placeForSleep;
    private Category category;
    private int indexForCategory;
    private double costPerDay;
    private RoomStatus roomStatus;
    private long indexForRoomDeletion;

    @BeforeClass
    public static void initRandom() {
        random = new Random();
    }

    @Before
    public void initDataAndDAO() {
        id = random.nextLong();
        roomNumber = random.nextInt();
        placeForSleep = random.nextInt();
        indexForCategory = random.nextInt(2);
        category = Category.values()[indexForCategory];
        costPerDay = random.nextDouble();
        roomStatus = RoomStatus.FREE;
        roomDAO = new RoomDAO();
    }

    @Test
    public void addRoomTest() throws TechnicalException{
        Room room = new Room();
        room.setId(id);
        room.setRoomNumber(roomNumber);
        room.setPlaceForSleep(placeForSleep);
        room.setCategory(category);
        room.setCostPerDay(costPerDay);
        room.setRoomStatus(roomStatus);
        ArrayList<Room> roomsFromDAO = new ArrayList<>();
        try {
            roomDAO.createRoom(room);
            roomsFromDAO = roomDAO.findAllRoom();
        } catch (DAOException e) {
            throw new TechnicalException();
        }
        Assert.assertNotSame(roomsFromDAO.get(roomsFromDAO.size() - 1), room);
    }

    @Test
    public void deleteRoomTest() throws DAOException {
        ArrayList<Room> roomsFromDAO = roomDAO.findAllRoom();
        indexForRoomDeletion = roomsFromDAO.get(roomsFromDAO.size() - 1).getId();
        roomDAO.deleteRoomById(indexForRoomDeletion);
        Room room = roomDAO.findRoomById(indexForRoomDeletion);
        Assert.assertNull(room);
    }
}
