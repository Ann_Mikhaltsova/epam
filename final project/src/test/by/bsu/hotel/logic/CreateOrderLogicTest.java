package test.by.bsu.hotel.logic;

import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.CreateOrderLogic;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;

public class CreateOrderLogicTest {
    @Test
    public void checkDateTest() throws TechnicalException {
        Date dateIn = Date.valueOf("2016-04-09");
        Date dateOut = Date.valueOf("2016-04-08");
        Assert.assertTrue("Date of arrival later than date of departure",
                CreateOrderLogic.checkDate(dateIn, dateOut));
    }
}
