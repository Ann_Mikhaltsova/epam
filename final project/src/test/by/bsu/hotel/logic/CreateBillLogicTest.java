package test.by.bsu.hotel.logic;

import by.bsu.hotel.dao.IOrderDAO;
import by.bsu.hotel.dao.OrderDAO;
import by.bsu.hotel.entity.Order;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.CreateBillLogic;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class CreateBillLogicTest {
    private IOrderDAO orderDAO;
    Order order;

    @Before
    public void initDataAndDAO() throws TechnicalException {
        orderDAO = new OrderDAO();
        ArrayList<Order> ordersFromDAO = new ArrayList<>();
        try {
            ordersFromDAO = orderDAO.findAllOrders();
            order = orderDAO.findAllOrders().get(ordersFromDAO.size() - 1);
        } catch (DAOException e) {
            throw new TechnicalException();
        }
    }

    @Test
    public void calculateTotalCostTest() {
        double days = ((order.getDateOut().getTime() - order.getDateIn().getTime()) / (24 * 60 * 60 * 1000)) + 1;
        double expectedCost = days * order.getRoom().getCostPerDay();
        double actualCost = CreateBillLogic.calculateTotalCost(order);
        Assert.assertEquals(expectedCost, actualCost, 0.01);
    }
}
