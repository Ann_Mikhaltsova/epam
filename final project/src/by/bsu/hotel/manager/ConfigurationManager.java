package by.bsu.hotel.manager;

import java.util.ResourceBundle;

public class ConfigurationManager {
	private static ConfigurationManager instance;
	private ResourceBundle resourceBundle;
	private static final String BUNDLE = "resources.config";
	public static final String DB_URL = "db.url";
	public static final String DB_USER = "db.user";
	public static final String DB_PASSWORD = "db.password";
	public static final String DB_MAXCONN = "db.maxConnections";

	public static final String LOG4J_PATH = "log4j.path";
	public static final String ERROR_PAGE_PATH = "error.page.path";
	public static final String INDEX_PAGE_PATH = "index.page.path";
	public static final String LOGIN_PAGE_PATH = "login.page.path";
	public static final String REGISTRATION_PAGE_PATH = "registration.page.path";
	public static final String CLIENT_PAGE_PATH = "client.page.path";
	public static final String CREATE_ORDER_PATH = "create.order.page.path";
	public static final String CLIENT_ORDER_LIST_PATH = "client.order.list.page.path";
	public static final String BILL_LIST_PATH = "bill.list.path";
	public static final String ADMIN_PAGE_PATH = "admin.page.path";
	public static final String ROOM_ADMINISTRATION_PATH = "room.administration.path";
	public static final String ORDER_LIST_PATH = "order.list.path";
	public static final String ADD_ROOM_PAGE_PATH = "addroom.page.path";

	public static final String LOGIN_IS_NOT_VALID = "login.is.not.valid";
	public static final String PASSWORD_IS_NOT_VALID = "password.is.not.valid";
	public static final String EMAIL_IS_NOT_VALID = "email.is.not.valid";
	public static final String NAME_IS_NOT_VALID = "name.is.not.valid";
	public static final String SURNAME_IS_NOT_VALID = "surname.is.not.valid";
	public static final String PATRONYMIC_IS_NOT_VALID = "patronymic.is.not.valid";

	public static final String REGISTRATION_WAS_SUCCESSFUL_MESSAGE = "registration.was.successful";
	public static final String REGISTRATION_WAS_INTERRUPT_MESSAGE = "registration.was.interrupt";
	public static final String LOGIC_EXCEPTION_ERROR_MESSAGE = "logic.exception.error.message";
	public static final String LOGIN_ERROR_MESSAGE = "login.error.message";
	public static final String DOES_NOT_HAVE_FREE_ROOM_MESSAGE = "does.not.have.free.rooms";
	public static final String EMPTY_ORDER_LIST = "empty.order.list";
	public static final String EMPTY_BILL_LIST = "empty.bill.list";
	public static final String WRONG_DATE_EXCEPTION_MESSAGE = "wrong.date.exception.message";
	public static final String CROSS_DATE_EXCEPTION_MESSAGE = "cross.date.exception.message";
	public static final String CREATE_ORDER_SUCCESS_MESSAGE = "create.order.success.message";
	public static final String CREATE_ORDER_PROBLEM_MESSAGE = "create.order.problem";
	public static final String DELETE_ORDER_SUCCESS_MESSAGE ="delete.order.success.message";
	public static final String DELETE_ORDER_PROBLEM_MESSAGE = "delete.order.problem";
	public static final String DELETE_CONFIRMED_ORDER_MESSAGE = "delete.confirmed.order.message";
	public static final String DOES_NOT_HAVE_BILL_MESSAGE = "does.not.have.bill.message";
	public static final String PAYMENT_SUCCESS_MESSAGE = "payment.success.message";
	public static final String BILL_PAID_ERROR_MESSAGE = "bill.paid.error.message";
	public static final String BILL_ALREADY_PAID_MESSAGE = "bill.already.paid.message";
	public static final String CHANGE_STATUS_SUCCESS_MESSAGE = "change.status.success.message";
	public static final String CHOOSE_ACTION_ERROR_MESSAGE = "choose.action.error.message";
	public static final String CHANGE_ROOM_STATUS_ERROR_MESSAGE = "change.room.status.error.message";
	public static final String CHOOSE_STATUS_ERROR_MESSAGE = "choose.status.error.message";
	public static final String CHANGE_ORDER_STATUS_ERROR_MESSAGE = "change.order.status.error.message";
	public static final String NOT_ENOUGH_MONEY_MESSAGE = "not.enough.money.message";
	public static final String CHOOSE_SAME_ROOM_ERROR_MESSAGE = "choose.same.room.error.message";
	public static final String CHOOSE_EXISTED_ROOM_ERROR_MESSAGE = "choose.existed.room.error.message";
	public static final String CHANGE_ROOM_NUMBER_ERROR_MESSAGE = "change.room.number.error.message";

	public static final String CHANGE_ROOM_NUMBER_SUCCESS_MESSAGE = "change.room.number.success.message";
	public static final String CREATE_ROOM_SUCCESS_MESSAGE = "create.room.success.message";



	public static ConfigurationManager getInstance() {
		if (instance == null) {
			instance = new ConfigurationManager();
			instance.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
		}
		return instance;
	}

	public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

	public void setResourceBundle(ResourceBundle resourceBundle) {
		this.resourceBundle = resourceBundle;
	}

	public String getProperty(String property) {
		if (resourceBundle == null) {
			instance.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
		}
		return (String) resourceBundle.getObject(property);
	}
}
