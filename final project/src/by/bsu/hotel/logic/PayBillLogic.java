package by.bsu.hotel.logic;


import by.bsu.hotel.dao.BillDAO;
import by.bsu.hotel.dao.IBillDAO;
import by.bsu.hotel.dao.IUserDAO;
import by.bsu.hotel.dao.UserDAO;
import by.bsu.hotel.entity.Bill;
import by.bsu.hotel.entity.BillStatus;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;

import java.util.Random;

public class PayBillLogic {
	public static void payBill(String idBill) throws TechnicalException, LogicException {
		IBillDAO billDAO = new BillDAO();
		IUserDAO userDAO = new UserDAO();
		if (idBill != null) {
			try {
				Bill bill = billDAO.findBillById(Long.parseLong(idBill));
				if (bill.getBillStatus().equals(BillStatus.PAID)) {
					throw new LogicException(ConfigurationManager.getInstance().getProperty(
							ConfigurationManager.BILL_ALREADY_PAID_MESSAGE));
				}
				Double totalCost = bill.getTotalCost();
				Double clientBudget = bill.getOrder().getUser().getBudget();
				int attemptRaiseBudget = bill.getOrder().getUser().getAttemptRaiseBudget();
				while(attemptRaiseBudget > 0) {
					if(clientBudget < totalCost) {
						clientBudget += goToHappyBank();
					} else {
						break;
					}
					attemptRaiseBudget--;
				}
				bill.getOrder().getUser().setAttemptRaiseBudget(attemptRaiseBudget);
				if(attemptRaiseBudget == 0) {
					if(clientBudget >= totalCost) {
						bill.getOrder().getUser().setBudget(clientBudget - totalCost);
						userDAO.changeUserBudgetById(bill.getOrder().getUser().getId(), clientBudget - totalCost);
						billDAO.changeBillStatusById(Long.parseLong(idBill), BillStatus.PAID);
					} else {
						throw new LogicException(ConfigurationManager.getInstance().getProperty(
								ConfigurationManager.NOT_ENOUGH_MONEY_MESSAGE));
					}
				} else {
					bill.getOrder().getUser().setBudget(clientBudget - totalCost);
					userDAO.changeUserBudgetById(bill.getOrder().getUser().getId(), clientBudget - totalCost);
					billDAO.changeBillStatusById(Long.parseLong(idBill), BillStatus.PAID);
				}

			} catch (DAOException e) {
				throw new TechnicalException();
			}
		} else {
			throw new LogicException(ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.BILL_PAID_ERROR_MESSAGE));
		}
	}

	private static double goToHappyBank() {
		Random random = new Random();
		double minLiberalitySum = 100;
		double maxLiberalitySum = 1000;
		return (maxLiberalitySum - minLiberalitySum) * random.nextDouble() + 100;
	}
}
