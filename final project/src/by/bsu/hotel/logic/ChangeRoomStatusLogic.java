package by.bsu.hotel.logic;


import by.bsu.hotel.dao.IRoomDAO;
import by.bsu.hotel.dao.RoomDAO;
import by.bsu.hotel.entity.Room;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;

public class ChangeRoomStatusLogic {
	private static final String PARAM_STATUS_EMPTY = "empty";

	public static void changeRoomStatus(String idRoom, String roomStatus) throws TechnicalException, LogicException {
		IRoomDAO roomDAO = new RoomDAO();
		if (idRoom != null) {
			if (PARAM_STATUS_EMPTY.equals(roomStatus)) {
				throw new LogicException(ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.CHOOSE_ACTION_ERROR_MESSAGE));
			}
			try {
				Room room = roomDAO.findRoomById(Long.parseLong(idRoom));
				if(RoomStatus.valueOf(roomStatus).equals(room.getRoomStatus())) {
					throw new LogicException(ConfigurationManager.getInstance().getProperty(
							ConfigurationManager.CHOOSE_STATUS_ERROR_MESSAGE));
				}
				roomDAO.changeRoomStatus(Long.parseLong(idRoom), RoomStatus.valueOf(roomStatus));
			} catch (DAOException e) {
				throw new TechnicalException();
			}
		} else {
			throw new LogicException(ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.CHANGE_ROOM_STATUS_ERROR_MESSAGE));
		}
	}
}
