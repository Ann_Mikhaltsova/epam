package by.bsu.hotel.logic;

import by.bsu.hotel.dao.IRoomDAO;
import by.bsu.hotel.dao.RoomDAO;
import by.bsu.hotel.entity.Room;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.TechnicalException;

import java.util.ArrayList;

public class FindRoomLogic {
	public static ArrayList<Room> findFreeRoom() throws TechnicalException {
		IRoomDAO roomDAO = new RoomDAO();
		ArrayList<Room> freeRoom = new ArrayList<>();
		try {
			for (Room room : roomDAO.findAllRoom()) {
				if (!room.getRoomStatus().equals(RoomStatus.BUSY))
					freeRoom.add(room);
			}
		} catch (DAOException e) {
			throw new TechnicalException();
		}
		return freeRoom;
	}

	public static ArrayList<Room> findAllRoom() throws TechnicalException {
		IRoomDAO roomDAO = new RoomDAO();
		ArrayList<Room> listRoom = new ArrayList<>();
		try {
			listRoom = roomDAO.findAllRoom();
		} catch (DAOException e) {
			throw new TechnicalException();
		}
		return listRoom;
	}
}
