package by.bsu.hotel.logic;

import by.bsu.hotel.dao.IRoomDAO;
import by.bsu.hotel.dao.RoomDAO;
import by.bsu.hotel.entity.Room;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;

import java.util.ArrayList;

public class ChangeRoomNumberLogic {
    public static void changeRoomNumber(String idRoom, String roomNumber) throws TechnicalException, LogicException {
        IRoomDAO roomDAO = new RoomDAO();
        if (idRoom != null) {
            try {
                Room room = roomDAO.findRoomById(Long.parseLong(idRoom));
                if(Integer.parseInt(roomNumber) == room.getRoomNumber()) {
                    throw new LogicException(ConfigurationManager.getInstance().getProperty(
                            ConfigurationManager.CHOOSE_SAME_ROOM_ERROR_MESSAGE));
                }
                ArrayList<Room> roomList = FindRoomLogic.findAllRoom();
                if(isRoomNumberExist(roomList, Integer.parseInt(roomNumber))) {
                    throw new LogicException(ConfigurationManager.getInstance().getProperty(
                            ConfigurationManager.CHOOSE_EXISTED_ROOM_ERROR_MESSAGE));
                }
                roomDAO.changeRoomNumber(Long.parseLong(idRoom), Integer.parseInt(roomNumber));
            } catch (DAOException e) {
                throw new TechnicalException();
            }
        } else {
            throw new LogicException(ConfigurationManager.getInstance().getProperty(
                    ConfigurationManager.CHANGE_ROOM_NUMBER_ERROR_MESSAGE));
        }
    }

    public static boolean isRoomNumberExist(ArrayList<Room> roomList, int roomNumber) {
        boolean roomNumberExist = false;
        for(int i = 0; i < roomList.size(); i++) {
            if(roomList.get(i).getRoomNumber() == roomNumber) {
                roomNumberExist = true;
            }
        }
        return roomNumberExist;
    }
}
