package by.bsu.hotel.logic;

import by.bsu.hotel.dao.IOrderDAO;
import by.bsu.hotel.dao.OrderDAO;
import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.OrderStatus;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;

public class ChangeStatusOrderLogic {
	public static final String PARAM_STATUS_EMPTY = "empty";

	public static void changeStatusOrder(String idOrder, String orderStatus) throws TechnicalException, LogicException {
		IOrderDAO orderDAO = new OrderDAO();
		if (idOrder != null) {
			if (PARAM_STATUS_EMPTY.equals(orderStatus)) {
				throw new LogicException(ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.CHOOSE_ACTION_ERROR_MESSAGE));
			}
			try {
				Order order = orderDAO.findOrderById(Long.parseLong(idOrder));
				if(OrderStatus.valueOf(orderStatus).equals(order.getOrderStatus())) {
					throw new LogicException(ConfigurationManager.getInstance().getProperty(
							ConfigurationManager.CHOOSE_STATUS_ERROR_MESSAGE));
				}
				orderDAO.changeOrderStatusById(Long.parseLong(idOrder), OrderStatus.valueOf(orderStatus));
				if (OrderStatus.valueOf(orderStatus).equals(OrderStatus.CONFIRMED)) {
					CreateBillLogic.createBill(idOrder);
				}
			} catch (DAOException e) {
				throw new TechnicalException();
			}
		} else {
			throw new LogicException(ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.CHANGE_ORDER_STATUS_ERROR_MESSAGE));
		}
	}
}
