package by.bsu.hotel.logic;

import by.bsu.hotel.dao.IRoomDAO;
import by.bsu.hotel.dao.RoomDAO;
import by.bsu.hotel.entity.Category;
import by.bsu.hotel.entity.Room;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class CreateRoomLogic {
    public static final Logger LOG = Logger.getLogger(CreateRoomLogic.class);
    public static final String PARAM_ERROR_MESSAGE = "errorMessage";
    private static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";

    public static void createRoom(Room room, String roomNumber, String placeForSleep,
                                  String category, String costPerDay) throws TechnicalException, LogicException {
        IRoomDAO roomDAO = new RoomDAO();
        ArrayList<Room> roomList = FindRoomLogic.findAllRoom();
        if(ChangeRoomNumberLogic.isRoomNumberExist(roomList, Integer.parseInt(roomNumber))) {
            throw new LogicException(ConfigurationManager.getInstance().getProperty(
                    ConfigurationManager.CHOOSE_EXISTED_ROOM_ERROR_MESSAGE));
        }
        room.setRoomNumber(Integer.parseInt(roomNumber));
        room.setPlaceForSleep(Integer.parseInt(placeForSleep));
        room.setCategory(Category.valueOf(category));
        room.setCostPerDay(Double.parseDouble(costPerDay));
        try {
            roomDAO.createRoom(room);
        } catch (DAOException e) {
            throw new TechnicalException();
        }
    }
}
