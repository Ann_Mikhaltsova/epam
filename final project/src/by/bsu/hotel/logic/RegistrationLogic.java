package by.bsu.hotel.logic;


import by.bsu.hotel.dao.IUserDAO;
import by.bsu.hotel.dao.UserDAO;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import java.util.regex.Pattern;

public class RegistrationLogic {
	public static final Logger LOG = Logger.getLogger(RegistrationLogic.class);

	private static final String PARAM_LOGIN_VALIDATION = "^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z\\d]{4,19}$";
	private static final String PARAM_PASSWORD_VALIDATION = "^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z0-9-_\\.]{4,19}$";
	private static final String PARAM_EMAIL_VALIDATION =
			"^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$";
	private static final String PARAM_NAME_VALIDATION = "^[A-ZА-ЯЁ][a-zа-яё]{2,19}$";

	public static void registration(User user, String password, String passwordAgain)
			throws TechnicalException, LogicException {
		IUserDAO userDAO = new UserDAO();
		try {
			validation(user, password, passwordAgain);
			LOG.info(userDAO.findUserByLoginPassword(user.getLogin(), user.getPassword()));
			if (userDAO.findUserByLoginPassword(user.getLogin(), user.getPassword()) == null) {
				userDAO.addUser(user);
			} else {
				throw new LogicException(ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.REGISTRATION_WAS_INTERRUPT_MESSAGE));
			}
		} catch (DAOException e) {
			throw new TechnicalException();
		}
	}

	private static void validation(User user, String password, String passwordAgain) throws LogicException {
		validationField(PARAM_LOGIN_VALIDATION, ConfigurationManager.LOGIN_IS_NOT_VALID, user.getLogin());
		validationPassword(password, passwordAgain);
		validationField(PARAM_EMAIL_VALIDATION, ConfigurationManager.EMAIL_IS_NOT_VALID, user.getEmail());
		validationField(PARAM_NAME_VALIDATION,ConfigurationManager.NAME_IS_NOT_VALID, user.getName());
		validationField(PARAM_NAME_VALIDATION, ConfigurationManager.SURNAME_IS_NOT_VALID, user.getSurname());
		validationField(PARAM_NAME_VALIDATION, ConfigurationManager.PATRONYMIC_IS_NOT_VALID, user.getPatronymic());
	}

	private static void validationPassword(String password, String passwordAgain) throws LogicException {
		if (!password.equals(passwordAgain) && Pattern.matches(PARAM_PASSWORD_VALIDATION, password)) {
			throw new LogicException(ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.PASSWORD_IS_NOT_VALID));
		}
	}

	private static void validationField(String pattern, String errorMessage, String field) throws LogicException {
		if (!Pattern.matches(pattern, field)) {
			throw new LogicException(ConfigurationManager.getInstance().getProperty(errorMessage));
		}
	}
}
