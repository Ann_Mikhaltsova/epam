package by.bsu.hotel.logic;


import by.bsu.hotel.dao.BillDAO;
import by.bsu.hotel.dao.IBillDAO;
import by.bsu.hotel.entity.Bill;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.TechnicalException;

import java.util.ArrayList;

public class FindBillLogic {
	public static ArrayList<Bill> findClientBill(long idUser) throws TechnicalException {
		IBillDAO billDAO = new BillDAO();
		ArrayList<Bill> billList = new ArrayList<>();
		try {
			for (Bill bill : billDAO.findUserBills(idUser)) {
				billList.add(bill);
			}
		} catch (DAOException e) {
			throw new TechnicalException();
		}
		return billList;
	}
}
