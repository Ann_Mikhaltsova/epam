package by.bsu.hotel.logic;


import by.bsu.hotel.dao.IOrderDAO;
import by.bsu.hotel.dao.OrderDAO;
import by.bsu.hotel.entity.Order;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.TechnicalException;

import java.util.ArrayList;


public class FindOrderLogic {
	public static ArrayList<Order> findClientOrder(long idUser) throws TechnicalException {
		IOrderDAO orderDAO = new OrderDAO();
		ArrayList<Order> orderList = new ArrayList<>();
		try {
			for (Order order : orderDAO.findUserOrders(idUser)) {
				orderList.add(order);
			}
		} catch (DAOException e) {
			throw new TechnicalException();
		}
		return orderList;
	}

	public static ArrayList<Order> findAllOrder() throws TechnicalException {
		IOrderDAO orderDAO = new OrderDAO();
		try {
			return orderDAO.findAllOrders();
		} catch (DAOException e) {
			throw new TechnicalException();
		}
	}
}
