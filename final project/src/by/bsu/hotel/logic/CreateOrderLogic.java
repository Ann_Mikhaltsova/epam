package by.bsu.hotel.logic;

import by.bsu.hotel.dao.IOrderDAO;
import by.bsu.hotel.dao.IRoomDAO;
import by.bsu.hotel.dao.OrderDAO;
import by.bsu.hotel.dao.RoomDAO;
import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class CreateOrderLogic {
	public static final Logger LOG = Logger.getLogger(CreateOrderLogic.class);
	public static final String PARAM_ERROR_MESSAGE = "errorMessage";
	private static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";

	public static void createOrder(Order order, User user, String idRoom, String dateIn, String dateOut)
			throws TechnicalException, LogicException {
		SimpleDateFormat format = new SimpleDateFormat(PARAM_DATE_FORMAT);
		IOrderDAO orderDAO = new OrderDAO();
		IRoomDAO roomDAO = new RoomDAO();
		if (idRoom != null) {
			try {
				order.setDateIn(new Date(format.parse(dateIn).getTime()));
				order.setDateOut(new Date(format.parse(dateOut).getTime()));
			} catch (ParseException e) {
				throw new TechnicalException();
			}
			if (checkDate(order.getDateIn(), order.getDateOut())) {
				if(!checkCrossDate(Integer.parseInt(idRoom), order.getDateIn(), order.getDateOut())) {
					order.setUser(user);
					try {
						order.setRoom(roomDAO.findRoomById(Long.parseLong(idRoom)));
						order.getRoom().setRoomStatus(RoomStatus.BUSY);
						//roomDAO.changeRoomStatus(Long.parseLong(idRoom), order.getRoom().getRoomStatus());
						orderDAO.createOrder(order);
					} catch (DAOException e) {
						throw new TechnicalException();
					}
				} else {
					throw new LogicException(ConfigurationManager.getInstance().getProperty(
							ConfigurationManager.CROSS_DATE_EXCEPTION_MESSAGE));
				}
			} else {
				throw new LogicException(ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.WRONG_DATE_EXCEPTION_MESSAGE));
			}
		} else {
			throw new LogicException(ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.CREATE_ORDER_PROBLEM_MESSAGE));
		}

	}

	public static boolean checkDate(Date dateIn, Date dateOut) throws TechnicalException {
		return dateOut.getTime() - dateIn.getTime() > 0;
	}

	private static boolean checkCrossDate(long idRoom, Date dateIn, Date dateOut) throws TechnicalException{
		IOrderDAO orderDAO = new OrderDAO();
		boolean crossDate = false;
		try {
			ArrayList<Order> orderList = orderDAO.findRoomOrders(idRoom);
			for(int i = 0; i < orderList.size(); i++) {
				Date tempForDateIn = orderList.get(i).getDateIn();
				Date tempForDateOut = orderList.get(i).getDateOut();
				if(!((dateIn.getTime() < tempForDateIn.getTime() && dateOut.getTime() <= tempForDateIn.getTime())
						|| (dateIn.getTime() >= tempForDateOut.getTime()
							&& dateOut.getTime() > tempForDateOut.getTime()))) {
					crossDate = true;
				}
			}
		} catch (DAOException e) {
			throw new TechnicalException();
		}
		return crossDate;
	}
}
