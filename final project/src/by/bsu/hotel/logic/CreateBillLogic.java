package by.bsu.hotel.logic;

import by.bsu.hotel.dao.*;
import by.bsu.hotel.entity.Bill;
import by.bsu.hotel.entity.BillStatus;
import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.TechnicalException;


public class CreateBillLogic {
	public static void createBill(String idOrder) throws TechnicalException {
		try {
			IBillDAO billDAO = new BillDAO();
			IOrderDAO orderDAO = new OrderDAO();
			Bill bill = new Bill();
			Order order = orderDAO.findOrderById(Integer.parseInt(idOrder));
			bill.setOrder(order);
			bill.setTotalCost(calculateTotalCost(order));
			bill.setBillStatus(BillStatus.UNPAID);
			setBusyRoom(order.getRoom().getId());
			billDAO.createBill(bill);
		} catch (DAOException e) {
			throw new TechnicalException();
		}
	}

	private static void setBusyRoom(long idRoom) throws DAOException {
		IRoomDAO roomDAO = new RoomDAO();
		roomDAO.changeRoomStatus(idRoom, RoomStatus.BUSY);
	}


	public static double calculateTotalCost(Order order) {
		double days = ((order.getDateOut().getTime() - order.getDateIn().getTime()) / (24 * 60 * 60 * 1000)) + 1;
		double totalCost = days * order.getRoom().getCostPerDay();
		return totalCost;
	}
}
