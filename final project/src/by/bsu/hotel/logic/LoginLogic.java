package by.bsu.hotel.logic;

import by.bsu.hotel.dao.IUserDAO;
import by.bsu.hotel.dao.UserDAO;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.TechnicalException;

public class LoginLogic {
	public static User checkLogin(String login, String password) throws TechnicalException {
		IUserDAO userDAO = new UserDAO();
		User user = null;
		try {
			user = userDAO.findUserByLoginPassword(login, password);
		} catch (DAOException e) {
			throw new TechnicalException();
		}
		return user;
	}
}
