package by.bsu.hotel.logic;


import by.bsu.hotel.dao.IOrderDAO;
import by.bsu.hotel.dao.IRoomDAO;
import by.bsu.hotel.dao.OrderDAO;
import by.bsu.hotel.dao.RoomDAO;
import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.OrderStatus;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.manager.ConfigurationManager;

public class DeleteOrderLogic {
	public static final String PARAM_ERROR_MESSAGE = "errorMessage";

	public static void deleteOrder(String idOrder) throws TechnicalException, LogicException {
		IOrderDAO orderDAO = new OrderDAO();
		IRoomDAO roomDAO = new RoomDAO();
		if (idOrder != null) {
			try {
				Order order = orderDAO.findOrderById(Long.parseLong(idOrder));
				if (order.getOrderStatus().equals(OrderStatus.CONFIRMED)) {
					throw new LogicException(ConfigurationManager.getInstance().getProperty(
							ConfigurationManager.DELETE_CONFIRMED_ORDER_MESSAGE));
				}
				orderDAO.deleteOrderById(Long.parseLong(idOrder));
				roomDAO.changeRoomStatus(order.getRoom().getId(), RoomStatus.FREE);
			} catch (DAOException e) {
				throw new TechnicalException();
			}
		} else {
			throw new LogicException(ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.DELETE_ORDER_PROBLEM_MESSAGE));
		}
	}
}
