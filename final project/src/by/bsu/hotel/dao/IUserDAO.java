package by.bsu.hotel.dao;

import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.DAOException;

import java.util.ArrayList;

public interface IUserDAO {
	User findUserByLoginPassword(String login, String password) throws DAOException;

	User findUserById(long idUser) throws DAOException;

	void addUser(User user) throws DAOException;

	ArrayList<User> findAllUser() throws DAOException;

	void changeUserBudgetById(long idUser, double budget) throws DAOException;
}
