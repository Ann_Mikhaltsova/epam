package by.bsu.hotel.dao;

import by.bsu.hotel.entity.Room;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.DAOException;

import java.util.ArrayList;


public interface IRoomDAO {
	void createRoom(Room room) throws DAOException;

	ArrayList<Room> findAllRoom() throws DAOException;

	Room findRoomById(long idRoom) throws DAOException;

	void changeRoomStatus(long idRoom, RoomStatus roomStatus) throws DAOException;

	void changeRoomNumber(long idRoom, int roomNumber) throws DAOException;

	void deleteRoomById(long idRoom) throws DAOException;
}
