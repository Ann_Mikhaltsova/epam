package by.bsu.hotel.dao;

import by.bsu.hotel.entity.AccessLevel;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class UserDAO extends AbstractDAO implements IUserDAO {

	public static final Logger LOG = Logger.getLogger(UserDAO.class);

	public static final String PARAM_ID_USER = "idUser";
	public static final String PARAM_LOGIN = "login";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_NAME = "name";
	public static final String PARAM_SURNAME = "surname";
	public static final String PARAM_PATRONYMIC = "patronymic";
	public static final String PARAM_BIRTHDAY = "birthday";
	public static final String PARAM_BUDGET = "budget";
	public static final String PARAM_ACCESS_LEVEL = "accessLevel";

	private static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";

	public static final String GET_USER_BY_LOGIN_PASSWORD = "SELECT * FROM `user` "
			+ "WHERE login = ? AND password = ?";

	public static final String GET_USER_BY_ID = "SELECT * FROM `user` "
			+ "WHERE idUser = ?";

	public static final String INSERT_USER = "INSERT INTO `user`"
			+ "(idUser, login, password, email, name, surname, patronymic, birthday) "
			+ "VALUES(?,?,?,?,?,?,?,?)";

	public static final String GET_ALL_USER = "SELECT * FROM `user` ";

	public static final String UPDATE_USER_BUDGET = "UPDATE `user` "
			+ "SET budget = ?" + "WHERE idUser =?";

	public User findUserByLoginPassword(String login, String password) throws DAOException {
		SimpleDateFormat format = new SimpleDateFormat(PARAM_DATE_FORMAT);
		User user = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN_PASSWORD);
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				user = new User();
				user.setId(Long.parseLong(resultSet.getString(PARAM_ID_USER)));
				user.setLogin(resultSet.getString(PARAM_LOGIN));
				user.setPassword(resultSet.getString(PARAM_PASSWORD));
				user.setEmail(resultSet.getString(PARAM_EMAIL));
				user.setName(resultSet.getString(PARAM_NAME));
				user.setSurname(resultSet.getString(PARAM_SURNAME));
				user.setPatronymic(resultSet.getString(PARAM_PATRONYMIC));
				try {
					user.setBirthday(new Date(format.parse(resultSet.getString(PARAM_BIRTHDAY)).getTime()));
				} catch (ParseException e) {
					LOG.warn("Parser's problems");
				}
				user.setBudget(Integer.parseInt(resultSet.getString(PARAM_BUDGET)));
				user.setAccess(AccessLevel.valueOf(resultSet.getString(PARAM_ACCESS_LEVEL).toUpperCase()));
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return user;
	}

	@Override
	public User findUserById(long idUser) throws DAOException {
		SimpleDateFormat format = new SimpleDateFormat(PARAM_DATE_FORMAT);
		User user = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(GET_USER_BY_ID);
			preparedStatement.setLong(1, idUser);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getLong(PARAM_ID_USER));
				user.setLogin(resultSet.getString(PARAM_LOGIN));
				user.setPassword(resultSet.getString(PARAM_PASSWORD));
				user.setEmail(resultSet.getString(PARAM_EMAIL));
				user.setName(resultSet.getString(PARAM_NAME));
				user.setSurname(resultSet.getString(PARAM_SURNAME));
				user.setPatronymic(resultSet.getString(PARAM_PATRONYMIC));
				try {
					user.setBirthday(new Date(format.parse(resultSet.getString(PARAM_BIRTHDAY)).getTime()));
				} catch (ParseException e) {
					LOG.warn("Parser's problems");
				}
				user.setBudget(Integer.parseInt(resultSet.getString(PARAM_BUDGET)));
				user.setAccess(AccessLevel.valueOf(resultSet.getString(PARAM_ACCESS_LEVEL)));
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return user;
	}

	@Override
	public void addUser(User user) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(INSERT_USER);
			preparedStatement.setLong(1, user.getId());
			preparedStatement.setString(2, user.getLogin());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.setString(4, user.getEmail());
			preparedStatement.setString(5, user.getName());
			preparedStatement.setString(6, user.getSurname());
			preparedStatement.setString(7, user.getPatronymic());
			preparedStatement.setDate(8, user.getBirthday());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public ArrayList<User> findAllUser() throws DAOException {
		SimpleDateFormat format = new SimpleDateFormat(PARAM_DATE_FORMAT);
		ArrayList<User> userList = new ArrayList<>();
		User user = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(GET_ALL_USER);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getLong(PARAM_ID_USER));
				user.setLogin(resultSet.getString(PARAM_LOGIN));
				user.setPassword(resultSet.getString(PARAM_PASSWORD));
				user.setEmail(resultSet.getString(PARAM_EMAIL));
				user.setName(resultSet.getString(PARAM_NAME));
				user.setSurname(resultSet.getString(PARAM_SURNAME));
				user.setPatronymic(resultSet.getString(PARAM_PATRONYMIC));
				try {
					user.setBirthday(new Date(format.parse(resultSet.getString(PARAM_BIRTHDAY)).getTime()));
				} catch (ParseException e) {
					LOG.warn("Parser's problems");
				}
				user.setBudget(resultSet.getInt(PARAM_BUDGET));
				user.setAccess(AccessLevel.valueOf(resultSet.getString(PARAM_ACCESS_LEVEL)));
				userList.add(user);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return userList;
	}

	@Override
	public void changeUserBudgetById(long idUser, double budget) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(UPDATE_USER_BUDGET);
			preparedStatement.setDouble(1, budget);
			preparedStatement.setLong(2, idUser);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}

	}
}
