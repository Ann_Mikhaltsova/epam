package by.bsu.hotel.dao;


import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.OrderStatus;
import by.bsu.hotel.exception.DAOException;

import java.util.ArrayList;


public interface IOrderDAO {
	void createOrder(Order order) throws DAOException;

	ArrayList<Order> findAllOrders() throws DAOException;

	ArrayList<Order> findUserOrders(long idUser) throws DAOException;

	ArrayList<Order> findRoomOrders(long idRoom) throws DAOException;

	void deleteOrderById(long idOrder) throws DAOException;

	void changeOrderStatusById(long idOrder, OrderStatus orderStatus) throws DAOException;

	Order findOrderById(long idOrder) throws DAOException;
}
