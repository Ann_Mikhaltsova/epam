package by.bsu.hotel.dao;


import by.bsu.hotel.entity.Bill;
import by.bsu.hotel.entity.BillStatus;
import by.bsu.hotel.exception.DAOException;

import java.util.ArrayList;

public interface IBillDAO {
	void createBill(Bill bill) throws DAOException;
	ArrayList<Bill> findAllBills() throws DAOException;
	ArrayList<Bill> findUserBills(long idUser) throws DAOException;
	void changeBillStatusById(long idBill, BillStatus status) throws DAOException;
	Bill findBillById(long idBill) throws DAOException;
}
