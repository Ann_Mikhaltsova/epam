package by.bsu.hotel.dao;

import by.bsu.hotel.entity.Bill;
import by.bsu.hotel.entity.BillStatus;
import by.bsu.hotel.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class BillDAO extends AbstractDAO implements IBillDAO {
	public static final Logger LOG = Logger.getLogger(BillDAO.class);

	public static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";
	public static final String PARAM_BILL_ID = "idBill";
	public static final String PARAM_BILL_STATUS = "billStatus";
	public static final String PARAM_BILL_TOTAL_PRICE = "totalCost";

	public static final String PARAM_BILL_ORDER_ID = "idOrder";

	public static final String INSERT_BILL = "INSERT INTO `bill` (idOrder, totalCost, billStatus) VALUES(?,?,?)";

	private static final String FIND_ALL_BILLS = "SELECT * FROM `bill`";

	public static final String FIND_USER_BILLS = "SELECT * FROM `bill` "
			+ "JOIN `order` ON bill.idOrder = order.idOrder "
			+ "WHERE idUser = ?";

	private static final String UPDATE_BILL_STATUS = "UPDATE `bill` SET billStatus = ? WHERE idBill = ?";

	private static final String FIND_BILL_BY_ID = "SELECT * FROM `bill` WHERE idBill = ?";

	@Override
	public Bill findBillById(long idBill) throws DAOException {
		Bill bill = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(FIND_BILL_BY_ID);
			preparedStatement.setLong(1, idBill);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				bill = new Bill();
				IOrderDAO orderDAO = new OrderDAO();
				bill.setId(resultSet.getLong(PARAM_BILL_ID));
				bill.setOrder(orderDAO.findOrderById(resultSet.getLong(PARAM_BILL_ORDER_ID)));
				bill.setTotalCost(resultSet.getDouble(PARAM_BILL_TOTAL_PRICE));
				bill.setBillStatus(BillStatus.values()[resultSet.getInt(PARAM_BILL_STATUS)]);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return bill;
	}

	@Override
	public void createBill(Bill bill) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(INSERT_BILL);
			preparedStatement.setLong(1, bill.getOrder().getId());
			preparedStatement.setDouble(2, bill.getTotalCost());
			preparedStatement.setInt(3, bill.getBillStatus().ordinal());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public ArrayList<Bill> findAllBills() throws DAOException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ArrayList<Bill> billList = new ArrayList<>();
		IOrderDAO orderDAO = new OrderDAO();
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(FIND_ALL_BILLS);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Bill bill = new Bill();
				bill.setId(resultSet.getLong(PARAM_BILL_ID));
				bill.setOrder(orderDAO.findOrderById(resultSet.getLong(PARAM_BILL_ORDER_ID)));
				bill.setTotalCost(resultSet.getDouble(PARAM_BILL_TOTAL_PRICE));
				bill.setBillStatus(BillStatus.valueOf(resultSet.getString(PARAM_BILL_STATUS)));
				billList.add(bill);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return billList;
	}

	@Override
	public void changeBillStatusById(long idBill, BillStatus billStatus) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(UPDATE_BILL_STATUS);
			preparedStatement.setInt(1, billStatus.ordinal());
			preparedStatement.setLong(2, idBill);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public ArrayList<Bill> findUserBills(long idUser) throws DAOException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		IOrderDAO orderDAO = new OrderDAO();
		ArrayList<Bill> billList = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(FIND_USER_BILLS);
			preparedStatement.setLong(1, idUser);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Bill bill = new Bill();
				bill.setId(resultSet.getLong(PARAM_BILL_ID));
				bill.setOrder(orderDAO.findOrderById(resultSet.getLong(PARAM_BILL_ORDER_ID)));
				bill.setTotalCost(resultSet.getDouble(PARAM_BILL_TOTAL_PRICE));
				bill.setBillStatus(BillStatus.values()[resultSet.getInt(PARAM_BILL_STATUS)]);
				billList.add(bill);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return billList;
	}
}
