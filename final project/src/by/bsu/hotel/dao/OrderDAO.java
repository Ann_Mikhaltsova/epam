package by.bsu.hotel.dao;


import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.OrderStatus;
import by.bsu.hotel.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class OrderDAO extends AbstractDAO implements IOrderDAO {
	public static final Logger LOG = Logger.getLogger(OrderDAO.class);
	public static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";

	public static final String PARAM_ID_ORDER = "idOrder";
	public static final String PARAM_DATE_OF_ARRIVAL = "dateIn";
	public static final String PARAM_DATE_OF_DEPARTURE = "dateOut";
	public static final String PARAM_STATUS_ORDER = "orderStatus";

	public static final String PARAM_ID_USER = "idUser";
	public static final String PARAM_ID_ROOM = "idRoom";

	public static final String INSERT_ORDER = "INSERT INTO `order`"
			+ "(dateIn,dateOut,orderStatus,idRoom,idUser)"
			+ "VALUES(?,?,?,?,?)";
	public static final String GET_ALL_ORDERS = "SELECT * FROM `order`";

	public static final String GET_USER_ORDERS = "SELECT * FROM `order`"
			+ "WHERE idUser = ?";
	public static final String GET_ROOM_ORDERS = "SELECT * FROM `order`"
			+ "WHERE idRoom = ?";

	public static final String DELETE_ORDER_BY_ID = "DELETE FROM `order` "
			+ "WHERE idOrder = ?";

	public static final String UPDATE_ORDER_STATUS = "UPDATE `order` "
			+ "SET orderStatus = ?" + "WHERE idOrder =?";

	public static final String GET_ORDER_BY_ID = "SELECT * FROM `order`"
			+ "WHERE idOrder = ?";


	@Override
	public void createOrder(Order order) throws DAOException {
		PreparedStatement preparedStatement = null;
		DateFormat format = new SimpleDateFormat(PARAM_DATE_FORMAT);
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(INSERT_ORDER);
			preparedStatement.setDate(1, order.getDateIn());
			preparedStatement.setDate(2, order.getDateOut());
			preparedStatement.setString(3, order.getOrderStatus().toString());
			preparedStatement.setLong(4, order.getRoom().getId());
			preparedStatement.setLong(5, order.getUser().getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public ArrayList<Order> findAllOrders() throws DAOException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		IUserDAO userDAO = new UserDAO();
		IRoomDAO roomDAO = new RoomDAO();
		ArrayList<Order> orderList = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(GET_ALL_ORDERS);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Order order = new Order();
				order.setId(resultSet.getInt(PARAM_ID_ORDER));
				order.setDateIn(resultSet.getDate(PARAM_DATE_OF_ARRIVAL));
				order.setDateOut(resultSet.getDate(PARAM_DATE_OF_DEPARTURE));
				order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(PARAM_STATUS_ORDER)));
				order.setRoom(roomDAO.findRoomById(resultSet.getInt(PARAM_ID_ROOM)));
				order.setUser(userDAO.findUserById(resultSet.getInt(PARAM_ID_USER)));
				orderList.add(order);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return orderList;
	}


	@Override
	public void deleteOrderById(long idOrder) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(DELETE_ORDER_BY_ID);
			preparedStatement.setLong(1, idOrder);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public void changeOrderStatusById(long idOrder, OrderStatus orderStatus) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(UPDATE_ORDER_STATUS);
			preparedStatement.setString(1, orderStatus.toString());
			preparedStatement.setLong(2, idOrder);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public Order findOrderById(long idOrder) throws DAOException {
		Order order = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(GET_ORDER_BY_ID);
			preparedStatement.setLong(1, idOrder);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				order = new Order();
				IRoomDAO roomDAO = new RoomDAO();
				IUserDAO userDAO = new UserDAO();
				order.setId(resultSet.getLong(PARAM_ID_ORDER));
				order.setDateIn(resultSet.getDate(PARAM_DATE_OF_ARRIVAL));
				order.setDateOut(resultSet.getDate(PARAM_DATE_OF_DEPARTURE));
				order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(PARAM_STATUS_ORDER)));
				order.setRoom(roomDAO.findRoomById(resultSet.getLong(PARAM_ID_ROOM)));
				order.setUser(userDAO.findUserById(resultSet.getLong(PARAM_ID_USER)));
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return order;
	}

	@Override
	public ArrayList<Order> findUserOrders(long idUser) throws DAOException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		IUserDAO userDAO = new UserDAO();
		IRoomDAO roomDAO = new RoomDAO();
		ArrayList<Order> orderList = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(GET_USER_ORDERS);
			preparedStatement.setLong(1, idUser);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Order order = new Order();
				order.setId(resultSet.getLong(PARAM_ID_ORDER));
				order.setDateIn(resultSet.getDate(PARAM_DATE_OF_ARRIVAL));
				order.setDateOut(resultSet.getDate(PARAM_DATE_OF_DEPARTURE));
				order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(PARAM_STATUS_ORDER)));
				order.setRoom(roomDAO.findRoomById(resultSet.getLong(PARAM_ID_ROOM)));
				order.setUser(userDAO.findUserById(resultSet.getLong(PARAM_ID_USER)));
				orderList.add(order);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return orderList;
	}

	@Override
	public ArrayList<Order> findRoomOrders(long idRoom) throws DAOException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		IUserDAO userDAO = new UserDAO();
		IRoomDAO roomDAO = new RoomDAO();
		ArrayList<Order> orderList = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(GET_ROOM_ORDERS);
			preparedStatement.setLong(1, idRoom);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Order order = new Order();
				order.setId(resultSet.getLong(PARAM_ID_ORDER));
				order.setDateIn(resultSet.getDate(PARAM_DATE_OF_ARRIVAL));
				order.setDateOut(resultSet.getDate(PARAM_DATE_OF_DEPARTURE));
				order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(PARAM_STATUS_ORDER)));
				order.setRoom(roomDAO.findRoomById(resultSet.getLong(PARAM_ID_ROOM)));
				order.setUser(userDAO.findUserById(resultSet.getLong(PARAM_ID_USER)));
				orderList.add(order);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return orderList;
	}
}
