package by.bsu.hotel.dao;

import by.bsu.hotel.entity.Category;
import by.bsu.hotel.entity.Room;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RoomDAO extends AbstractDAO implements IRoomDAO {
	public static final Logger LOG = Logger.getLogger(RoomDAO.class);

	public static final String PARAM_ID = "idRoom";
	public static final String PARAM_ROOM_NUMBER = "roomNumber";
	public static final String PARAM_SLEEP_PLACE = "placeForSleep";
	public static final String PARAM_CATEGORY = "category";
	public static final String PARAM_COST_PER_DAY = "costPerDay";
	public static final String PARAM_ROOM_STATUS = "roomStatus";

	public static final String INSERT_ROOM = "INSERT INTO `room`"
			+ "(roomNumber, placeForSleep, category, costPerDay)"
			+ "VALUES(?,?,?,?)";
	public static final String FIND_ROOM_LIST = "SELECT * FROM `room` ";

	public static final String FIND_ROOM_BY_ID = "SELECT * FROM `room` WHERE idRoom=?";

	public static final String UPDATE_ROOM_STATUS = "UPDATE room SET roomStatus = ? WHERE idRoom = ? ";
	public static final String UPDATE_ROOM_NUMBER = "UPDATE room SET roomNumber = ? WHERE idRoom = ? ";
	public static final String DELETE_ROOM = "DELETE FROM `room` WHERE idRoom = ?";


	@Override
	public void createRoom(Room room) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(INSERT_ROOM);
			preparedStatement.setInt(1, room.getRoomNumber());
			preparedStatement.setInt(2, room.getPlaceForSleep());
			preparedStatement.setString(3, room.getCategory().toString());
			preparedStatement.setDouble(4, room.getCostPerDay());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}
	@Override
	public ArrayList<Room> findAllRoom() throws DAOException {
		ArrayList<Room> roomList = new ArrayList<Room>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(FIND_ROOM_LIST);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Room room = new Room();
				room.setId(Long.parseLong(resultSet.getString(PARAM_ID)));
				room.setRoomNumber(Integer.parseInt(resultSet.getString(PARAM_ROOM_NUMBER)));
				room.setPlaceForSleep(Integer.parseInt(resultSet.getString(PARAM_SLEEP_PLACE)));
				room.setCategory(Category.valueOf(resultSet.getString(PARAM_CATEGORY)));
				room.setCostPerDay(Double.parseDouble(resultSet.getString(PARAM_COST_PER_DAY)));
				room.setRoomStatus(RoomStatus.valueOf(resultSet.getString(PARAM_ROOM_STATUS)));
				roomList.add(room);
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return roomList;
	}

	@Override
	public Room findRoomById(long idRoom) throws DAOException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Room room = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(FIND_ROOM_BY_ID);
			preparedStatement.setLong(1, idRoom);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				room = new Room();
				room.setId(Long.parseLong(resultSet.getString(PARAM_ID)));
				room.setRoomNumber(Integer.parseInt(resultSet.getString(PARAM_ROOM_NUMBER)));
				room.setPlaceForSleep(Integer.parseInt(resultSet.getString(PARAM_SLEEP_PLACE)));
				room.setCategory(Category.valueOf(resultSet.getString(PARAM_CATEGORY)));
				room.setCostPerDay(Double.parseDouble(resultSet.getString(PARAM_COST_PER_DAY)));
				//room.setRoomStatus(RoomStatus.values()[(resultSet.getInt(PARAM_ROOM_STATUS) + 1) % 2]);
				room.setRoomStatus(RoomStatus.valueOf(resultSet.getString(PARAM_ROOM_STATUS)));
			}
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
		return room;
	}

	@Override
	public void changeRoomStatus(long idRoom, RoomStatus roomStatus) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(UPDATE_ROOM_STATUS);
			preparedStatement.setString(1, roomStatus.toString());
			preparedStatement.setLong(2, idRoom);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public void changeRoomNumber(long idRoom, int roomNumber) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(UPDATE_ROOM_NUMBER);
			preparedStatement.setInt(1, roomNumber);
			preparedStatement.setLong(2, idRoom);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}

	@Override
	public void deleteRoomById(long idRoom) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(DELETE_ROOM);
			preparedStatement.setLong(1, idRoom);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closePreparedStatement(preparedStatement);
			releaseConnection(connection);
		}
	}
}
