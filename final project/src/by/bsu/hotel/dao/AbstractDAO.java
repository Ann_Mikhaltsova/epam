package by.bsu.hotel.dao;

import by.bsu.hotel.exception.DAOException;
import by.bsu.hotel.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class AbstractDAO {
	public static final Logger LOG = Logger.getLogger(AbstractDAO.class);
	protected ConnectionPool pool;

	public AbstractDAO() {
		pool = ConnectionPool.getInstance();
	}

	public Connection getConnection() {
		return pool.getConnection();
	}

	public void releaseConnection(Connection connection) {
		if (connection != null) {
			ConnectionPool.getInstance().closeConnection(connection);
		}
	}

	public void closePreparedStatement(PreparedStatement preparedStatement)
			throws DAOException {
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				////;LOGnhklhnlknnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
			}
		}
	}
}
