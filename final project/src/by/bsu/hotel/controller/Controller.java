package by.bsu.hotel.controller;

import by.bsu.hotel.command.ActionCommand;
import by.bsu.hotel.command.CommandFactory;
import by.bsu.hotel.manager.ConfigurationManager;
import by.bsu.hotel.pool.ConnectionPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
	public static final Logger LOG = Logger.getLogger(Controller.class);

	public Controller() {
		super();
		LOG.info("Create 'Controller' object.");
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		new DOMConfigurator().doConfigure(getServletContext().getRealPath(
				ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOG4J_PATH)),
				LogManager.getLoggerRepository());
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("command");
		String page = null;
		ActionCommand command = CommandFactory.createCommand(action);
		page = command.execute(request);
		getServletContext().getRequestDispatcher(page).forward(request, response);
	}

	@Override
	public void destroy() {
		ConnectionPool.getInstance().releaseConnections();
		super.destroy();
	}
}
