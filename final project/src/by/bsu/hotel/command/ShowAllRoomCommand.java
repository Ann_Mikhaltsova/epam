package by.bsu.hotel.command;

import by.bsu.hotel.entity.Room;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.FindRoomLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class ShowAllRoomCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ShowAllRoomCommand.class);

	private static final String PARAM_LIST_ROOM = "roomList";
	private static final String PARAM_ERROR_MESSAGE = "errorMessage";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		ArrayList<Room> listRoom = null;
		try {
			listRoom = FindRoomLogic.findAllRoom();
			request.setAttribute(PARAM_LIST_ROOM, listRoom);
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ROOM_ADMINISTRATION_PATH);
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}
}
