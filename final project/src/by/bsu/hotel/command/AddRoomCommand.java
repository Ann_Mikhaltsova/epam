package by.bsu.hotel.command;

import by.bsu.hotel.entity.Room;
import by.bsu.hotel.entity.RoomStatus;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.CreateRoomLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class AddRoomCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(AddRoomCommand.class);

    private static final String PARAM_ERROR_MESSAGE = "errorMessage";
    public static final String PARAM_ACTION_MESSAGE = "actionMessage";

    private static final String PARAM_ROOM_NUMBER = "roomNumber";
    private static final String PARAM_PLACE_FOR_SLEEP = "placeForSleep";
    private static final String PARAM_CATEGORY = "category";
    private static final String PARAM_COST_PER_DAY = "costPerDay";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        Room room = new Room();
        room.setRoomStatus(RoomStatus.FREE);
        String roomNumber = request.getParameter(PARAM_ROOM_NUMBER);
        String placeForSleep = request.getParameter(PARAM_PLACE_FOR_SLEEP);
        String category = request.getParameter(PARAM_CATEGORY);
        String costPerDay = request.getParameter(PARAM_COST_PER_DAY);
        try {
            CreateRoomLogic.createRoom(room, roomNumber, placeForSleep, category, costPerDay);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADD_ROOM_PAGE_PATH);
            request.setAttribute(PARAM_ACTION_MESSAGE, ConfigurationManager.getInstance().getProperty(
                    ConfigurationManager.CREATE_ROOM_SUCCESS_MESSAGE));
        } catch (TechnicalException e) {
            LOG.error("Something has gone wrong, redirect to error page.", e);
            request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
                    ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
        } catch (LogicException e) {
            LOG.error("Something has gone wrong with creating order.", e);
            request.setAttribute(PARAM_ERROR_MESSAGE, e);
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ROOM_ADMINISTRATION_PATH);
        }
        return page;
    }
}
