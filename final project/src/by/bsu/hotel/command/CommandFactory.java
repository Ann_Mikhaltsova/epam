package by.bsu.hotel.command;

public class CommandFactory {
    public static ActionCommand createCommand(String action) {
        if (action != null)
            switch (action) {
                case "language":
                    return new LanguageCommand();
                case "registration":
                    return new RegistrationCommand();
                case "login":
                    return new LoginCommand();
                case "logout":
                    return new LogOutCommand();
                case "showFreeRoom":
                    return new ShowFreeRoomCommand();
                case "createOrder":
                    return new CreateOrderCommand();
                case "showMyOrder":
                    return new ShowClientOrderCommand();
                case "deleteOrder":
                    return new DeleteOrderCommand();
                case "showMyBill":
                    return new ShowClientBillCommand();
                case "payment":
                    return new PaymentCommand();
                case "roomAdministration":
                    return new ShowAllRoomCommand();
                case "setStatusRoom":
                    return new ChangeRoomStatusCommand();
                case "orderAdministration":
                    return new OrderAdministrationCommand();
                case "setStatusOrder":
                    return new ChangeStatusOrderCommand();
                case "changeRoomNumber":
                    return new ChangeRoomNumberCommand();
                case "addRoom":
                    return new AddRoomCommand();
               case "page":
                    return new PageCommand();
               default:
                    return new NoCommand();
            }
        return new NoCommand();
    }
}
