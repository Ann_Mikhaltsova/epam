package by.bsu.hotel.command;


import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.OrderStatus;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.CreateOrderLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class CreateOrderCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(CreateOrderCommand.class);
	//private static final String PARAM_FREE_ROOM = "freeRoom";

	private static final String PARAM_ERROR_MESSAGE = "errorMessage";
	public static final String PARAM_ACTION_MESSAGE = "actionMessage";

	private static final String PARAM_DATE_OF_ARRIVAL = "dateIn";
	private static final String PARAM_DATE_OF_DEPARTURE = "dateOut";
	private static final String PARAM_ROOM_ID = "idRoom";

	private static final String PARAM_USER = "user";


	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Order order = new Order();
		User user = (User) request.getSession().getAttribute(PARAM_USER);
		order.setOrderStatus(OrderStatus.PENDING);
		String idRoom = request.getParameter(PARAM_ROOM_ID);
		String dateIn = request.getParameter(PARAM_DATE_OF_ARRIVAL);
		String dateOut = request.getParameter(PARAM_DATE_OF_DEPARTURE);
		try {
			CreateOrderLogic.createOrder(order, user, idRoom, dateIn, dateOut);
			//ArrayList<Room> freeRoom = FindRoomLogic.findFreeRoom();
			//request.setAttribute(PARAM_FREE_ROOM, freeRoom);
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PAGE_PATH);
			//page = refreshWithChanges(request);
			request.setAttribute(PARAM_ACTION_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.CREATE_ORDER_SUCCESS_MESSAGE));
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		} catch (LogicException e) {
			LOG.error("Something has gone wrong with creating order.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, e);
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CREATE_ORDER_PATH);
			//page = refreshWithChanges(request);
		}
		return page;
	}

	/*private String refreshWithChanges(HttpServletRequest request) {
		ArrayList<Room> freeRoom;
		try {
			freeRoom = FindRoomLogic.findFreeRoom();
			request.setAttribute(PARAM_FREE_ROOM, freeRoom);
		} catch (TechnicalException e) {
			LOG.error("Something goes wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.CREATE_ORDER_PATH);
	}*/
}

