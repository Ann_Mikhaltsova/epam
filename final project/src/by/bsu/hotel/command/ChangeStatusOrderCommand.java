package by.bsu.hotel.command;


import by.bsu.hotel.entity.Order;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.ChangeStatusOrderLogic;
import by.bsu.hotel.logic.FindOrderLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class ChangeStatusOrderCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ChangeStatusOrderCommand.class);

	private static final String PARAM_ORDER_LIST = "orderList";
	private static final String PARAM_ORDER_ID = "idOrder";
	private static final String PARAM_ORDER_STATUS = "orderStatus";

	private static final String PARAM_ERROR_MESSAGE = "errorMessage";
	public static final String PARAM_ACTION_MESSAGE = "actionMessage";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String idOrder = request.getParameter(PARAM_ORDER_ID);
		String orderStatus = request.getParameter(PARAM_ORDER_STATUS);
		try {
			ChangeStatusOrderLogic.changeStatusOrder(idOrder, orderStatus);
			page = refreshWithChanges(request);
			request.setAttribute(PARAM_ACTION_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.CHANGE_STATUS_SUCCESS_MESSAGE));
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		} catch (LogicException e) {
			if(ConfigurationManager.getInstance().getProperty(ConfigurationManager.CHOOSE_STATUS_ERROR_MESSAGE).
					equals(e.getMessage())) {
				request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.CHOOSE_STATUS_ERROR_MESSAGE));
			} else if(ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.CHOOSE_ACTION_ERROR_MESSAGE).equals(e.getMessage())){
				request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.CHOOSE_ACTION_ERROR_MESSAGE));
			} else {
				request.setAttribute(PARAM_ERROR_MESSAGE, e);
			}
			LOG.error("Something has gone wrong with changing order status.", e);
			page = refreshWithChanges(request);
		}
		return page;
	}

	private String refreshWithChanges(HttpServletRequest request) {
		ArrayList<Order> orderList;
		try {
			orderList = FindOrderLogic.findAllOrder();
			request.setAttribute(PARAM_ORDER_LIST, orderList);
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ORDER_LIST_PATH);
	}
}
