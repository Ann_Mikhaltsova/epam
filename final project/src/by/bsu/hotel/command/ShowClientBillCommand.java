package by.bsu.hotel.command;


import by.bsu.hotel.entity.Bill;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.FindBillLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class ShowClientBillCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ShowClientBillCommand.class);

	private static final String PARAM_CLIENT_BILL = "clientBill";
	private static final String PARAM_USER = "user";
	private static final String PARAM_ERROR_MESSAGE = "errorMessage";
	private static final String PARAM_EMPTY_BILL_LIST_MESSAGE = "emptyBill";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		ArrayList<Bill> clientBill = null;
		User user = (User) request.getSession().getAttribute(PARAM_USER);
		try {
			clientBill = FindBillLogic.findClientBill(user.getId());
			if (clientBill.size() != 0) {
				request.setAttribute(PARAM_CLIENT_BILL, clientBill);
			} else {
				request.setAttribute(PARAM_EMPTY_BILL_LIST_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.EMPTY_BILL_LIST));
			}
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.BILL_LIST_PATH);
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}
}
