package by.bsu.hotel.command;

import by.bsu.hotel.entity.Bill;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.FindBillLogic;
import by.bsu.hotel.logic.PayBillLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public class PaymentCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(PaymentCommand.class);

	private static final String PARAM_CLIENT_BILL = "clientBill";
	private static final String PARAM_BILL_ID = "idBill";
	private static final String PARAM_USER = "user";

	private static final String PARAM_ERROR_MESSAGE = "errorMessage";
	private static final String PARAM_ACTION_MESSAGE = "actionMessage";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String idBill = request.getParameter(PARAM_BILL_ID);
		try {
			PayBillLogic.payBill(idBill);
			page = refreshWithChanges(request);
			request.setAttribute(PARAM_ACTION_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.PAYMENT_SUCCESS_MESSAGE));
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		} catch (LogicException e) {
			if(ConfigurationManager.getInstance().getProperty(ConfigurationManager.NOT_ENOUGH_MONEY_MESSAGE).
					equals(e.getMessage())) {
				request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.NOT_ENOUGH_MONEY_MESSAGE));
			}  else {
				request.setAttribute(PARAM_ERROR_MESSAGE, e);
			}
			LOG.error("Something has gone wrong with bill payment.", e);
			page = refreshWithChanges(request);
		}
		return page;
	}

	private String refreshWithChanges(HttpServletRequest request) {
		ArrayList<Bill> clientBill = null;
		User user = (User) request.getSession().getAttribute(PARAM_USER);
		try {
			clientBill = FindBillLogic.findClientBill(user.getId());
			request.setAttribute(PARAM_CLIENT_BILL, clientBill);
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.BILL_LIST_PATH);
	}
}
