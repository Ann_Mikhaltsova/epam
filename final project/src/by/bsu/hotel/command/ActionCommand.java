package by.bsu.hotel.command;

import javax.servlet.http.HttpServletRequest;


public interface ActionCommand {

    String execute(HttpServletRequest request);
}
