package by.bsu.hotel.command;

import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.DeleteOrderLogic;
import by.bsu.hotel.logic.FindOrderLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class DeleteOrderCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(DeleteOrderCommand.class);

	private static final String PARAM_CLIENT_ORDER = "clientOrder";
	private static final String PARAM_ORDER_ID = "idOrder";
	private static final String PARAM_USER = "user";

	private static final String PARAM_ERROR_MESSAGE = "errorMessage";
	public static final String PARAM_ACTION_MESSAGE = "actionMessage";
	private static final String PARAM_EMPTY_ORDER_LIST_MESSAGE = "emptyOrder";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String idOrder = request.getParameter(PARAM_ORDER_ID);
		try {
			DeleteOrderLogic.deleteOrder(idOrder);
			page = refreshWithChanges(request);
			request.setAttribute(PARAM_ACTION_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.DELETE_ORDER_SUCCESS_MESSAGE));
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		} catch (LogicException e) {
			LOG.error("Something has gone wrong with deleting order.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, e);
			page = refreshWithChanges(request);
		}
		return page;
	}

	private String refreshWithChanges(HttpServletRequest request) {
		ArrayList<Order> clientOrder = null;
		User user = (User) request.getSession().getAttribute(PARAM_USER);
		try {
			clientOrder = FindOrderLogic.findClientOrder(user.getId());
			if (clientOrder.size() != 0) {
				request.setAttribute(PARAM_CLIENT_ORDER, clientOrder);
			}else {
				request.setAttribute(PARAM_EMPTY_ORDER_LIST_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.EMPTY_ORDER_LIST));
			}
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_ORDER_LIST_PATH);
	}
}
