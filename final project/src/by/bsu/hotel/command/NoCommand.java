package by.bsu.hotel.command;


import by.bsu.hotel.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class NoCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.INDEX_PAGE_PATH);
		return page;
	}
}
