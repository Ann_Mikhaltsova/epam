package by.bsu.hotel.command;

import by.bsu.hotel.encryption.MD5;
import by.bsu.hotel.entity.AccessLevel;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.LoginLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(LoginCommand.class);
	private static final String PARAM_USER = "user";
	private static final String PARAM_LOGIN = "login";
	private static final String PARAM_PASSWORD = "password";
	private static final String PARAM_ERROR_MESSAGE = "errorMessage";

	@Override
	public String execute(HttpServletRequest request) {
		MD5 md5 = new MD5();
		HttpSession session = request.getSession(true);
		String page = null;
		User user = null;
		String login = request.getParameter(PARAM_LOGIN);
		String password = md5.encipherPassword(request.getParameter(PARAM_PASSWORD));
		/*try {
			if ((user = LoginLogic.checkLogin(login, password)) != null) {
				LOG.info("Add to session user.");
				session.setAttribute(PARAM_USER, user);
				page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PAGE_PATH);
			} else {
				request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.LOGIN_ERROR_MESSAGE));
				page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
			}
		} catch (TechnicalException e) {
			LOG.error("Something goes wrong, redirect to error page.", e);
			request.setAttribute("errorMessage", ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}*/

		try {
			if ((user = LoginLogic.checkLogin(login, password)) != null) {
				LOG.info("Add to session user.");
				session.setAttribute(PARAM_USER, user);
				page = checkAccessLevel(user.getAccess(), request);
			} else {
				request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.LOGIN_ERROR_MESSAGE));
				page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
			}
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute("errorMessage", ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}

	private String checkAccessLevel(AccessLevel access, HttpServletRequest request) {
		AccessLevel check = access;
		if (check != null) {
			switch (check) {
				case ADMIN:
					return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_PAGE_PATH);
				case CLIENT:
					return ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PAGE_PATH);
			}
		}
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
	}
}
