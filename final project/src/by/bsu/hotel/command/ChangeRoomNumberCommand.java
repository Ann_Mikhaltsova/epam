package by.bsu.hotel.command;

import by.bsu.hotel.entity.Room;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.ChangeRoomNumberLogic;
import by.bsu.hotel.logic.FindRoomLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class ChangeRoomNumberCommand implements ActionCommand {

    private static final Logger LOG = Logger.getLogger(ChangeRoomNumberCommand.class);
    private static final String PARAM_ROOM_LIST = "roomList";
    private static final String PARAM_ROOM_ID = "idRoom";
    private static final String PARAM_ROOM_NUMBER = "roomNumber";

    private static final String PARAM_ERROR_MESSAGE = "errorMessage";
    private static final String PARAM_ACTION_MESSAGE = "actionMessage";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String idRoom = request.getParameter(PARAM_ROOM_ID);
        String roomNumber = request.getParameter(PARAM_ROOM_NUMBER);
        try {
            ChangeRoomNumberLogic.changeRoomNumber(idRoom, roomNumber);
            page = refreshWithChanges(request);
            request.setAttribute(PARAM_ACTION_MESSAGE, ConfigurationManager.getInstance().getProperty(
                    ConfigurationManager.CHANGE_ROOM_NUMBER_SUCCESS_MESSAGE));
        } catch (TechnicalException e) {
            LOG.error("Something has gone wrong, redirect to error page.", e);
            request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
                    ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
        } catch (LogicException e) {
            LOG.error("Something has gone wrong with deleting order.", e);
            request.setAttribute(PARAM_ERROR_MESSAGE, e);
            page = refreshWithChanges(request);
        }
        return page;
    }

    private String refreshWithChanges(HttpServletRequest request) {
        ArrayList<Room> roomList;
        try {
            roomList = FindRoomLogic.findAllRoom();
            request.setAttribute(PARAM_ROOM_LIST, roomList);
        } catch (TechnicalException e) {
            LOG.error("Something has gone wrong, redirect to error page.", e);
            request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
                    ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
            return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
        }
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ROOM_ADMINISTRATION_PATH);
    }
}
