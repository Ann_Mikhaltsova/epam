package by.bsu.hotel.command;


import by.bsu.hotel.entity.Order;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.FindOrderLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class ShowClientOrderCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ShowClientOrderCommand.class);
	private static final String PARAM_CLIENT_ORDER = "clientOrder";
	private static final String PARAM_USER = "user";
	private static final String PARAM_ERROR_MESSAGE = "errorMessage";
	private static final String PARAM_EMPTY_ORDER_LIST_MESSAGE = "emptyOrder";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		ArrayList<Order> clientOrder = null;
		User user = (User) request.getSession().getAttribute(PARAM_USER);
		try {
			clientOrder = FindOrderLogic.findClientOrder(user.getId());
			if (clientOrder.size() != 0) {
				request.setAttribute(PARAM_CLIENT_ORDER, clientOrder);
			} else {
				request.setAttribute(PARAM_EMPTY_ORDER_LIST_MESSAGE, ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.EMPTY_ORDER_LIST));
			}
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_ORDER_LIST_PATH);
		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}
}
