package by.bsu.hotel.command;

import by.bsu.hotel.encryption.MD5;
import by.bsu.hotel.entity.AccessLevel;
import by.bsu.hotel.entity.User;
import by.bsu.hotel.exception.LogicException;
import by.bsu.hotel.exception.TechnicalException;
import by.bsu.hotel.logic.RegistrationLogic;
import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RegistrationCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(RegistrationCommand.class);
	private static final String PARAM_USER_LOGIN = "login";
	private static final String PARAM_USER_PASSWORD = "password";
	private static final String PARAM_USER_PASSWORD_AGAIN = "passwordagain";
	private static final String PARAM_USER_EMAIL = "email";
	private static final String PARAM_USER_NAME = "name";
	private static final String PARAM_USER_SURNAME = "surname";
	private static final String PARAM_USER_PATRONYMIC = "patronymic";
	private static final String PARAM_USER_BIRTHDAY = "birthday";

	private static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";

	private static final String PARAM_ACTION_MESSAGE = "actionMessage";
	private static final String PARAM_ERROR_MESSAGE = "errorMessage";

	@Override
	public String execute(HttpServletRequest request) {
		MD5 md5 = new MD5();
		SimpleDateFormat format = new SimpleDateFormat(PARAM_DATE_FORMAT);
		String page = null;
		User user = new User();
		String password = request.getParameter(PARAM_USER_PASSWORD);
		String passwordAgain = request.getParameter(PARAM_USER_PASSWORD_AGAIN);
		user.setLogin(request.getParameter(PARAM_USER_LOGIN));
		user.setPassword(md5.encipherPassword(password));
		user.setEmail(request.getParameter(PARAM_USER_EMAIL));
		user.setName(request.getParameter(PARAM_USER_NAME));
		user.setSurname(request.getParameter(PARAM_USER_SURNAME));
		user.setPatronymic(request.getParameter(PARAM_USER_PATRONYMIC));
		try {
			user.setBirthday(new Date(format.parse(request.getParameter(PARAM_USER_BIRTHDAY)).getTime()));
		} catch (ParseException e) {
			LOG.warn("Parser's problems");
		}
		user.setAccess(AccessLevel.CLIENT);
		try {
			RegistrationLogic.registration(user, password, passwordAgain);
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.INDEX_PAGE_PATH);
			request.setAttribute(PARAM_ACTION_MESSAGE, ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.REGISTRATION_WAS_SUCCESSFUL_MESSAGE));

		} catch (TechnicalException e) {
			LOG.error("Something has gone wrong, redirect to error page.", e);
			request.setAttribute( PARAM_ERROR_MESSAGE, ConfigurationManager.getInstance().getProperty(
							ConfigurationManager.LOGIC_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		} catch (LogicException e) {
			LOG.error("Something has gone wrong with registration.", e);
			request.setAttribute(PARAM_ERROR_MESSAGE, e);
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATION_PAGE_PATH);
		}
		return page;
	}
}
