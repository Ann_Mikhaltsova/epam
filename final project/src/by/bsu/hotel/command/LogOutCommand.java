package by.bsu.hotel.command;

import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class LogOutCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(LogOutCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		LOG.info("Finish session.");
		session.invalidate();
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.INDEX_PAGE_PATH);
	}
}
