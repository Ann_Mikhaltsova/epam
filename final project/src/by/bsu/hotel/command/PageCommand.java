package by.bsu.hotel.command;

import by.bsu.hotel.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class PageCommand implements ActionCommand {
	private static final String PARAM_PAGE = "page";
	private static final String PARAM_LOGIN_PAGE = "login";
	private static final String PARAM_REGISTRATION_PAGE = "registration";
	private static final String PARAM_CLIENT_PAGE = "clientpage";
	private static final String PARAM_ADMIN_PAGE = "adminpage";
	private static final String PARAM_ADD_ROOM_PAGE = "addroom";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		page = checkPage(request);
		return page;
	}

	private String checkPage(HttpServletRequest request) {
		String pageParam = request.getParameter(PARAM_PAGE);
		switch (pageParam) {
			case PARAM_LOGIN_PAGE:
				return ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
			case PARAM_REGISTRATION_PAGE:
				return ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATION_PAGE_PATH);
			case PARAM_CLIENT_PAGE:
				return ConfigurationManager.getInstance().getProperty(ConfigurationManager.CLIENT_PAGE_PATH);
			case PARAM_ADMIN_PAGE:
				return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_PAGE_PATH);
			case PARAM_ADD_ROOM_PAGE:
				return ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADD_ROOM_PAGE_PATH);
			default:
				return ConfigurationManager.getInstance().getProperty(ConfigurationManager.INDEX_PAGE_PATH);
		}
	}
}
