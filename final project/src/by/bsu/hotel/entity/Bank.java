package by.bsu.hotel.entity;

public class Bank {
    private long id;
    private int budget;

    public long getId() {
        return id;
    }

    public int getBudget() {
        return budget;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bank bank = (Bank) o;

        if (id != bank.id) return false;
        return budget == bank.budget;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + budget;
        return result;
    }
}
