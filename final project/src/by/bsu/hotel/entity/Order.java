package by.bsu.hotel.entity;


import java.sql.Date;

public class Order {
	private long id;
	private Date dateIn;
	private Date dateOut;
	private Room room;
	private User user;
	private OrderStatus orderStatus;

	public long getId() {
		return id;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public Date getDateOut() {
		return dateOut;
	}

	public Room getRoom() {
		return room;
	}

	public User getUser() {
		return user;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}

	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Order order = (Order) o;

		if (id != order.id) return false;
		if (!dateIn.equals(order.dateIn)) return false;
		if (!dateOut.equals(order.dateOut)) return false;
		if (!room.equals(order.room)) return false;
		if (!user.equals(order.user)) return false;
		return orderStatus == order.orderStatus;

	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + dateIn.hashCode();
		result = 31 * result + dateOut.hashCode();
		result = 31 * result + room.hashCode();
		result = 31 * result + user.hashCode();
		result = 31 * result + orderStatus.hashCode();
		return result;
	}
}
