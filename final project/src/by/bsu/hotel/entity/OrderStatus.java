package by.bsu.hotel.entity;

public enum OrderStatus {
	CONFIRMED,
	PENDING,
	DECLINED
}
