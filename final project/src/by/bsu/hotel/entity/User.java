package by.bsu.hotel.entity;


import java.sql.Date;

public class User{
	private long id;
	private String login;
	private String password;
	private String email;
	private String name;
	private String surname;
	private String patronymic;
	private Date birthday;
    private double budget;
    private AccessLevel access;
    int attemptRaiseBudget = 3;


    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Date getBirthday() {
        return birthday;
    }

    public double getBudget() {
        return budget;
    }

    public AccessLevel getAccess() {
        return access;
    }

    public int getAttemptRaiseBudget() {
        return attemptRaiseBudget;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public void setAccess(AccessLevel access) {
        this.access = access;
    }

    public void setAttemptRaiseBudget(int attemptRaiseBudget) {
        this.attemptRaiseBudget = attemptRaiseBudget;
    }

    @Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (Double.compare(user.budget, budget) != 0) return false;
        if (attemptRaiseBudget != user.attemptRaiseBudget) return false;
        if (!login.equals(user.login)) return false;
        if (!password.equals(user.password)) return false;
        if (!email.equals(user.email)) return false;
        if (!name.equals(user.name)) return false;
        if (!surname.equals(user.surname)) return false;
        if (!patronymic.equals(user.patronymic)) return false;
        if (!birthday.equals(user.birthday)) return false;
        return access == user.access;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + patronymic.hashCode();
        result = 31 * result + birthday.hashCode();
        temp = Double.doubleToLongBits(budget);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + access.hashCode();
        result = 31 * result + attemptRaiseBudget;
        return result;
    }
}
