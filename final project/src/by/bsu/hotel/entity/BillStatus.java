package by.bsu.hotel.entity;

public enum BillStatus {
	UNPAID,
	PAID
}
