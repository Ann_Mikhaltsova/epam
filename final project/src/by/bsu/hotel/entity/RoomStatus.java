package by.bsu.hotel.entity;

public enum RoomStatus {
	FREE,
	BUSY,
	REPAIR
}
