package by.bsu.hotel.entity;

public class Bill {
	private long id;
	private Order order;
	private double totalCost;
	private BillStatus billStatus;

	public long getId() {
		return id;
	}

	public Order getOrder() {
		return order;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public BillStatus getBillStatus() {
		return billStatus;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Bill bill = (Bill) o;

		if (id != bill.id) return false;
		if (Double.compare(bill.totalCost, totalCost) != 0) return false;
		if (!order.equals(bill.order)) return false;
		return billStatus == bill.billStatus;

	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = (int) (id ^ (id >>> 32));
		result = 31 * result + order.hashCode();
		temp = Double.doubleToLongBits(totalCost);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + billStatus.hashCode();
		return result;
	}
}
