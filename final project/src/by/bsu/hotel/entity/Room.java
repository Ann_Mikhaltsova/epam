package by.bsu.hotel.entity;

public class Room {
	private long id;
	private int roomNumber;
	private int placeForSleep;
	private Category category;
	private double costPerDay;
	private RoomStatus roomStatus;

	public long getId() {
		return id;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public int getPlaceForSleep() {
		return placeForSleep;
	}

	public Category getCategory() {
		return category;
	}

	public double getCostPerDay() {
		return costPerDay;
	}

	public RoomStatus getRoomStatus() {
		return roomStatus;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public void setPlaceForSleep(int placeForSleep) {
		this.placeForSleep = placeForSleep;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setCostPerDay(double costPerDay) {
		this.costPerDay = costPerDay;
	}

	public void setRoomStatus(RoomStatus roomStatus) {
		this.roomStatus = roomStatus;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Room room = (Room) o;

		if (id != room.id) return false;
		if (roomNumber != room.roomNumber) return false;
		if (placeForSleep != room.placeForSleep) return false;
		if (Double.compare(room.costPerDay, costPerDay) != 0) return false;
		if (category != room.category) return false;
		return roomStatus == room.roomStatus;

	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = (int) (id ^ (id >>> 32));
		result = 31 * result + roomNumber;
		result = 31 * result + placeForSleep;
		result = 31 * result + category.hashCode();
		temp = Double.doubleToLongBits(costPerDay);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + roomStatus.hashCode();
		return result;
	}
}
