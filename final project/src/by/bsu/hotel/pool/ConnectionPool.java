package by.bsu.hotel.pool;

import by.bsu.hotel.manager.ConfigurationManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    public static final Logger LOG = LogManager.getLogger(ConnectionPool.class.getName());
    private static ConnectionPool instance;
    private static BlockingQueue<Connection> pool;
    private static Lock lock = new ReentrantLock();
    private static ConfigurationManager config = ConfigurationManager.getInstance();
    private static AtomicBoolean flag = new AtomicBoolean(true);
    private boolean possibilityToTakeConnection = true;

    public static ConnectionPool getInstance() {
        if (flag.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    flag.set(false);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }


    /*if (instance == null) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new ConnectionPool();
                    // AtomicBoolean!!!
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;*/


    private ConnectionPool() {
        init();
    }

    private static void init() {
        LOG.info("Trying to create pool of connections...");
        String url = config.getProperty(ConfigurationManager.DB_URL);
        String user = config.getProperty(ConfigurationManager.DB_USER);
        String password = config.getProperty(ConfigurationManager.DB_PASSWORD);
        int size = Integer.parseInt(config.getProperty(ConfigurationManager.DB_MAXCONN));
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            pool = new ArrayBlockingQueue<Connection>(size);
            for (int i = 0; i < size; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                pool.offer(connection);
            }
            LOG.info("Connection pool successfully initialized.");
        } catch (SQLException e) {
            LOG.fatal("Error in initialization");
            throw new RuntimeException(e);
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        if (possibilityToTakeConnection) {
            try {
                connection = pool.take();
            } catch (InterruptedException e) {
                LOG.warn("Free connection waiting was interrupted.", e);
            }
        }
        return connection;
    }

    public void closeConnection(Connection connection) {
        try {
            if (!connection.isClosed()) {
                if (!pool.offer(connection)) {
                    LOG.warn("Connection has not added.");
                }
            } else {
                LOG.warn("Connection has already closed.");
            }
        } catch (SQLException e) {
            LOG.warn("SQLException in connection isClosed () checking. Connection had not added.", e);
        }
    }

    public void releaseConnections() {
        possibilityToTakeConnection = false;
        Connection connection = null;
        int realSize = Integer.parseInt(config.getProperty(ConfigurationManager.DB_MAXCONN));
        while (realSize > 0) {
            try {
                connection = pool.take();
            } catch (InterruptedException e) {
                LOG.warn("Waiting connection has interrupted exception. Return null connection.", e);
            }
            if (connection != null) {
                try {
                    if (!connection.isClosed()) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    LOG.warn("Problem with connection closing", e);
                }
                realSize--;
            }
        }
        LOG.info("Pool has successfully cleared.");
    }
}
