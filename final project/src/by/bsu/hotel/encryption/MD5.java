package by.bsu.hotel.encryption;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    public String encipherPassword(String password) {
        MessageDigest messageDigest = null;
        byte bytesEncoded[] = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-1"); // 1 раз !!!!!
            messageDigest.update(password.getBytes("utf8"));
            bytesEncoded = messageDigest.digest();
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, bytesEncoded);
        String resHex = bigInt.toString(16);
        return resHex;
    }
}
