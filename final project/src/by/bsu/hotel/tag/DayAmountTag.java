package by.bsu.hotel.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DayAmountTag extends TagSupport {
	private static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";
	private static final String PARAM_DAYS = "dayAmount";
	private String dateOut;
	private String dateIn;

	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}

	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}

	@Override
	public int doStartTag() throws JspException {
		DateFormat formater = new SimpleDateFormat(PARAM_DATE_FORMAT);
		try {
			double days;
			days = (formater.parse(dateOut).getTime() - formater.parse(dateIn).getTime()) / (24 * 60 * 60 * 1000)
					+ 1;
			pageContext.getRequest().setAttribute(PARAM_DAYS, days);
		} catch (ParseException e) {
			throw new JspException("Error: " + e.getMessage());
		}
		return SKIP_BODY;
	}
}
