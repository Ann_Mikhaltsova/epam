package by.bsu.oop.entity;


public abstract class Ingredient {
    private String name;
    private double calorie;
    private double weightInGram;

    public Ingredient(String name, double calorie, double weightInGram) {
        this.name = name;
        this.calorie = calorie;
        this.weightInGram = weightInGram;
    }

    public String getName() {
        return name;
    }

    public double getCalorie() {
        return calorie;
    }

    public double getWeightInGram() {
        return weightInGram;
    }

    @Override
    public String toString() {
        return "Name: '" + name + '\'' +
                ", Calorie: " + calorie +
                ", WeightInGram: " + weightInGram + " gr, ";
    }
}
