package by.bsu.oop.entity;


public class SunflowerOil extends Additive{
    private SunflowerOilType type;

    public SunflowerOil(String name, double calorie, double weightInGram, int keepingTimeInDays,
                        SunflowerOilType type) {
        super(name, calorie, weightInGram, keepingTimeInDays);
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Type: " + type + "\n";
    }
}
