package by.bsu.oop.entity;

import by.bsu.oop.action.RandomGenerator;
import by.bsu.oop.creator.IngredientCreator;
import by.bsu.oop.creator.SaladCreator;

import java.util.ArrayList;


public class Salad {
    private String name;
    private ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();

    public String getName() {
        return name;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public ArrayList<Ingredient> makeIngredientList() {
        ingredients = new IngredientCreator().init();
        return ingredients;
    }

    public void takeRecipe(){
        RandomGenerator randomGenerator = new RandomGenerator();
        SaladCreator saladCreator = new SaladCreator();
        name = randomGenerator.thinkSaladName();
        ArrayList<Ingredient> whatYouNeed = saladCreator.makeSalad(ingredients);
        ingredients.retainAll(whatYouNeed);
    }

    @Override
    public String toString() {
        return "Salad " +
                "'" + name + '\'' +
                ":\n\t Ingredients:\n" + ingredients;
    }
}
