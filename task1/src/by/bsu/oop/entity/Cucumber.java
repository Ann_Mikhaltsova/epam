package by.bsu.oop.entity;

/**
 * Created by Анна on 24.11.2015.
 */
public class Cucumber extends Vegetable {
    private int seedsNumber;

    public Cucumber(String name, double calorie, double weightInGram, LifeDuration lifeDuration, int seedsNumber) {
        super(name, calorie, weightInGram, lifeDuration);
        this.seedsNumber = seedsNumber;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SeedsNumber: " + seedsNumber + "\n";
    }
}
