package by.bsu.oop.entity;


public abstract class Additive extends Ingredient {
    private int keepingTimeInDays;

    public Additive(String name, double calorie, double weightInGram, int keepingTimeInDays) {
        super(name, calorie, weightInGram);
        this.keepingTimeInDays = keepingTimeInDays;
    }

    @Override
    public String toString() {
        return super.toString() +
                "KeepingTimeInDays: " + keepingTimeInDays + " days, ";
    }
}
