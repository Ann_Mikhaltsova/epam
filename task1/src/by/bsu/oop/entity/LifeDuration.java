package by.bsu.oop.entity;


public enum LifeDuration {
    PERENNIAL,
    BIENNIAL,
    ANNUAL
}
