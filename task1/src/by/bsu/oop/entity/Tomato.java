package by.bsu.oop.entity;


public class Tomato extends Vegetable {
    private String friutType;

    public Tomato(String name, double calorie, double weightInGram, LifeDuration lifeDuration, String friutType) {
        super(name, calorie, weightInGram, lifeDuration);
        this.friutType = friutType;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FriutType: '" + friutType + '\'' + "\n";
    }
}
