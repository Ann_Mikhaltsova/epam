package by.bsu.oop.entity;


public class Carrot extends Vegetable {
    private boolean blossomCluster; // наличие соцветия

    public Carrot(String name, double calorie, double weightInGram, LifeDuration lifeDuration,
                  boolean blossomCluster) {
        super(name, calorie, weightInGram, lifeDuration);
        this.blossomCluster = blossomCluster;
    }

    @Override
    public String toString() {
        return super.toString() +
                "BlossomCluster: " + blossomCluster + "\n";
    }
}

