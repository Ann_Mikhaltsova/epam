package by.bsu.oop.entity;


public class Mayonnaise extends Additive {
    private double fatContent;

    public Mayonnaise(String name, double calorie, double weightInGram, int keepingTimeInDays, double fatContent) {
        super(name, calorie, weightInGram, keepingTimeInDays);
        this.fatContent = fatContent;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FatContent: " + fatContent + "%" + "\n";
    }
}
