package by.bsu.oop.entity;


public class Lettuce extends Vegetable {
    private boolean folicAcidContent; // фолиевая кислота

    public Lettuce(String name, double calorie, double weightInGram, LifeDuration lifeDuration,
                   boolean folicAcidContent) {
        super(name, calorie, weightInGram, lifeDuration);
        this.folicAcidContent = folicAcidContent;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FolicAcidContent: " + folicAcidContent + "\n";
    }
}
