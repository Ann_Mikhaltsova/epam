package by.bsu.oop.entity;


public class Dill extends Vegetable {
    private boolean volatileProductionContent; // фитонциды

    public Dill(String name, double calorie, double weightInGram, LifeDuration lifeDuration,
                boolean volatileProductionContent) {
        super(name, calorie, weightInGram, lifeDuration);
        this.volatileProductionContent = volatileProductionContent;
    }

    @Override
    public String toString() {
        return super.toString() +
                "VolatileProductionContent: " + volatileProductionContent + "\n";
    }
}
