package by.bsu.oop.entity;


public abstract class Vegetable extends Ingredient {
    private LifeDuration lifeDuration;

    public Vegetable(String name, double calorie, double weightInGram, LifeDuration lifeDuration) {
        super(name, calorie, weightInGram);
        this.lifeDuration = lifeDuration;
    }

    @Override
    public String toString() {
        return super.toString() +
                "LifeDuration: " + lifeDuration + ", ";
    }
}
