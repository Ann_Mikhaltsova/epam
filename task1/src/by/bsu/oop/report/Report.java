package by.bsu.oop.report;

import by.bsu.oop.entity.Salad;
import by.bsu.oop.entity.Vegetable;

import java.util.ArrayList;

/**
 * Created by Анна on 24.11.2015.
 */
public class Report {
    public void printSalad(Salad salad) {
        System.out.println("==============================================================================" +
                            "======================================");
        System.out.println("Salad " + salad.getName());
        System.out.println("Ingredients: ");
        salad.getIngredients().forEach((ingredient)->System.out.print("\t" + ingredient));
        System.out.println("==============================================================================" +
                            "======================================");
    }

    public void printSaladCaloricity(double saladCaloricity) {
        System.out.println("Salad's caloricity: " + saladCaloricity);
    }

    public void printVegetableInCaloricityInterval(ArrayList<Vegetable> vegetablesInInterval) {
        if(!vegetablesInInterval.isEmpty()) {
            vegetablesInInterval.forEach((ingredient) -> System.out.print(ingredient));
        }
        else {
            System.out.println("There are no vegetables in such interval");
        }
    }
}
