package by.bsu.oop.exception;

public class CaloricityNegativeValueException extends Exception {
    public CaloricityNegativeValueException() {
    }

    public CaloricityNegativeValueException(String message) {
        super(message);
    }

    public CaloricityNegativeValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public CaloricityNegativeValueException(Throwable cause) {
        super(cause);
    }

    public CaloricityNegativeValueException(String message, Throwable cause, boolean enableSuppression,
                                            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
