package by.bsu.oop.creator;

import by.bsu.oop.action.RandomGenerator;
import by.bsu.oop.entity.Ingredient;

import java.util.ArrayList;

public class SaladCreator {
    public ArrayList<Ingredient> makeSalad(ArrayList<Ingredient> ingredients) {
        ArrayList<Ingredient> salad = new ArrayList<>();
        RandomGenerator randomGenerator = new RandomGenerator();
        salad = randomGenerator.chooseIngredients(ingredients);
        return salad;
    }
}
