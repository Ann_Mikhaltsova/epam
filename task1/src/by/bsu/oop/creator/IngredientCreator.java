package by.bsu.oop.creator;

import by.bsu.oop.entity.*;
import by.bsu.oop.exception.CaloricityNegativeValueException;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class IngredientCreator {
    private static final Logger LOG = Logger.getLogger(IngredientCreator.class);

    public ArrayList<Ingredient> init() {
        boolean flag = true;
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        SunflowerOil oil = new SunflowerOil("Sunflower oil", -884, 50, 27, SunflowerOilType.REFINED);
        Mayonnaise mayonnaise = new Mayonnaise("Mayonnaise", 680, 100, 31, 67);
        Lettuce lettuce = new Lettuce("Lettuce", 15, 600, LifeDuration.BIENNIAL, true);
        Carrot carrot = new Carrot("Carrot", 35, 3000, LifeDuration.BIENNIAL, true);
        Tomato tomato = new Tomato("Tomato", 18, 2600, LifeDuration.ANNUAL, "berry");
        Dill dill = new Dill("Dill", 40, 100, LifeDuration.ANNUAL, true);
        Cucumber cucumber = new Cucumber("Cucumber", 16, 2100, LifeDuration.ANNUAL, 127);
        ingredients.add(oil);
        ingredients.add(mayonnaise);
        ingredients.add(lettuce);
        ingredients.add(carrot);
        ingredients.add(tomato);
        ingredients.add(dill);
        ingredients.add(cucumber);
        for (int i = 0; i < ingredients.size(); i++) {
            try {
                if (ingredients.get(i).getCalorie() < 0) {
                    ingredients.remove(i);
                    throw new CaloricityNegativeValueException(ingredients.get(i--).getName());
                }
            } catch (CaloricityNegativeValueException ex) {
                LOG.error(ex.getMessage() + "'s caloricity has the negative value!");
            }
        }
        return ingredients;
    }
}
