package by.bsu.oop.main;

import by.bsu.oop.action.Computing;
import by.bsu.oop.action.Search;
import by.bsu.oop.action.Sorting;
import by.bsu.oop.entity.Ingredient;
import by.bsu.oop.entity.Salad;
import by.bsu.oop.report.Report;
import org.apache.log4j.Logger;

import java.util.ArrayList;


public class Chef {
    private static final Logger LOG = Logger.getLogger(Chef.class);

    public static void main(String[] args) {
        Salad salad = new Salad();
        ArrayList<Ingredient> ingredient = salad.makeIngredientList();
        salad.takeRecipe();
        Report report = new Report();
        report.printSalad(salad);
        LOG.info("Calculate salad's caloricity:");
        Computing computing = new Computing();
        report.printSaladCaloricity(computing.countCaloricity(salad));
        LOG.info("Sort:");
        Sorting sorting = new Sorting();
        sorting.doSortByWeight(salad.getIngredients());
        report.printSalad(salad);
        LOG.info("Search vegetables in certain caloricity interval:");
        Search search = new Search();
        report.printVegetableInCaloricityInterval(search.findVegetablesInCaloricityInterval(salad));
    }
}
