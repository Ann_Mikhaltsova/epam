package by.bsu.oop.action;

import by.bsu.oop.entity.Ingredient;
        import java.util.ArrayList;
        import java.util.Collections;

public class Sorting {
    public void doSortByWeight(ArrayList<Ingredient> ingredients) {
        Collections.sort(ingredients, (Ingredient i1, Ingredient i2)->
                (int)(i2.getWeightInGram() - i1.getWeightInGram()));

    }
}
