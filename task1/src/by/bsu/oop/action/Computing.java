package by.bsu.oop.action;

import by.bsu.oop.entity.Ingredient;
import by.bsu.oop.entity.Salad;

public class Computing {
    public double countCaloricity(Salad salad) {
        double caloricity = 0;
        for(Ingredient ingredient: salad.getIngredients()) {
            caloricity += (ingredient.getCalorie() * ingredient.getWeightInGram() / 100);
        }
        return caloricity;
    }
}
