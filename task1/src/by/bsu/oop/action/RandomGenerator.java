package by.bsu.oop.action;

import by.bsu.oop.entity.Ingredient;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Random;

public class RandomGenerator {
    public ArrayList<Ingredient> chooseIngredients(ArrayList<Ingredient> ingredients) {
        int minIngredientNumber = 3;
        Random random = new Random();
        int ingredientNumber = random.nextInt(ingredients.size() - 1 - minIngredientNumber) + minIngredientNumber;
        LinkedHashSet<Ingredient> salad = new LinkedHashSet<>(ingredientNumber);
        for(int i = 0; i < ingredientNumber; i++) {
            int randomIndex = random.nextInt(ingredients.size() - 1);
            if(!salad.add(ingredients.get(randomIndex))) {
                i--;
            }
        }
        return new ArrayList<>(salad);
    }

    public String thinkSaladName() {
        Random random = new Random();
        char[] capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        char[] lowers = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] vowels = "aeiou".toCharArray();
        String saladName = "";
        int syllableNumber = 4;
        saladName += capitals[random.nextInt(capitals.length)];
        for(int i = 0; i < syllableNumber; i++) {
            saladName += vowels[random.nextInt(vowels.length)];
            saladName += lowers[random.nextInt(lowers.length)];
        }
        return saladName;
    }
}
