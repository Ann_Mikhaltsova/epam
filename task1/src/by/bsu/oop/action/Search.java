package by.bsu.oop.action;

import by.bsu.oop.entity.Ingredient;
import by.bsu.oop.entity.Salad;
import by.bsu.oop.entity.Vegetable;

import java.util.ArrayList;

public class Search {
    private static final int MIN_INTERVAL_VALUE = 37;
    private static final int MAX_INTERVAL_VALUE = 700;

    public ArrayList<Vegetable> findVegetablesInCaloricityInterval(Salad salad) {
        ArrayList<Vegetable> vegetablesInInterval = new ArrayList<>();
        for (Ingredient ingredient : salad.getIngredients()) {
            if(isVegetable(ingredient) && isInInterval(ingredient)) {
                vegetablesInInterval.add((Vegetable) ingredient);
            }
        }
        return vegetablesInInterval;
    }

    private boolean isInInterval(Ingredient ingredient) {
        boolean flag = true;
        if(ingredient.getCalorie() < MIN_INTERVAL_VALUE && ingredient.getCalorie() > MAX_INTERVAL_VALUE) {
            flag = false;
        }
        return flag;
    }

    private boolean isVegetable (Ingredient ingredient) {
        boolean flag = true;
        if(!(ingredient instanceof Vegetable)) {
            flag = false;
        }
        return flag;
    }
}
